
import Foundation


struct ERROR {
    static let invalidUserName = "Invalid username"
    static let invalidPassword = "Invalid password"
}

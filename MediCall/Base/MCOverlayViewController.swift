

import UIKit

class MCOverlayViewController: UIViewController {

    convenience init() {
        self.init()
        self.modalPresentationStyle = UIModalPresentationStyle.popover
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.setupNavigationBar()
    }
    
    private func setupNavigationBar() {
        let navigationBar = self.navigationController?.navigationBar
        navigationBar?.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        navigationBar?.shadowImage = UIImage()
        self.navigationController?.navigationBar.isHidden = false;
        self.navigationController?.navigationBar.isTranslucent = false;
        self.navigationController?.navigationBar.configureForBlueBackground()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named:"btn_default_back.png"), style: .plain, target: self, action: #selector(dismisss))
    }
    
    @objc public func dismisss() {
        self.dismiss(completion: nil)
    }
    
    public func dismiss(completion: (() -> Void)?) {
        self.dismiss(animated: true, completion: completion);
    }

}

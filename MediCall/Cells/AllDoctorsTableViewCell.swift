
import UIKit

class AllDoctorsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var clCollectionView: UICollectionView!
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var drDesignationLbl: UILabel!
    @IBOutlet weak var drFeeLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var drProfileImageView: UIImageView!
    @IBOutlet weak var allDoctorsOuterView: UIView!
    
    @IBOutlet weak var allDoctorstopview: UIView!
    
    @IBOutlet weak var allDoctorsmiddleview: UIView!
    
    @IBOutlet weak var discountedImageView: UIImageView!
    @IBOutlet weak var allDoctorsBottomView: UIView!
    
    @IBOutlet weak var verifyDocImage: UIImageView!
    @IBOutlet weak var experienceLbl: UILabel!
    @IBOutlet weak var ratingBarLbl: UILabel!
    @IBOutlet weak var hospitalDescriptionLbl: UILabel!
    @IBOutlet weak var hospitalImage: UIImageView!
    
    @IBOutlet weak var distanceLbl: UILabel!
    
    @IBOutlet weak var distanceLblView: UIView!
    @IBOutlet weak var expLblView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        profileDescription.setCornerRadius(r: 8)
//        profileDescription.setBorderWidth(width: 1)
//        profileDescription.setBorderColor(color: UIColor(0xE2E2E2))
        distanceLblView.setCornerRadius(r: 5)
        //distanceLblView.setBorderWidth(width: 1)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
       // self.contentView.backgroundColor =  UIColor(patternImage: UIImage(named: "listingbackground")!)
        self.allDoctorsOuterView.setCornerRadius(r: 8)
        self.allDoctorsOuterView.setBorderWidth(width:0.5)
        self.allDoctorstopview.roundCorners([.topLeft,.topRight], radius:8)
        self.allDoctorsBottomView.roundCorners([.bottomLeft,.bottomRight], radius: 8)
      self.allDoctorsOuterView.setBorderColor(color: .white)
        self.hospitalImage.roundImage()
        self.drProfileImageView.contentMode = .scaleAspectFill
    }

}

extension AllDoctorsTableViewCell {
    func setCollectionViewDataSourceDelegate <D:UICollectionViewDelegate & UICollectionViewDataSource>
        (_ dataSourceDelegate:D , forRow row:Int)
    {
       clCollectionView.delegate = dataSourceDelegate
        clCollectionView.dataSource = dataSourceDelegate
        clCollectionView.reloadData()
    }
}

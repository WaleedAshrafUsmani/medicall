
import UIKit

class DoctorListInHospitalCell: UICollectionViewCell {
    @IBOutlet weak var doctorImage: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    
}

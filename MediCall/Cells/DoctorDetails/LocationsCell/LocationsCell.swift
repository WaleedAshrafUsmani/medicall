//
//  LocationsCell.swift
//  MediCall
//
//  Created by Muhammad Azher on 10/09/2018.
//  Copyright © 2018 Salman Saeed. All rights reserved.
//

import UIKit
import GoogleMaps

class LocationsCell: UITableViewCell {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var mapView: GMSMapView!
    
    
    // MARK: - Life Cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}

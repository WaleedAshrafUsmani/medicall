//
//  TimingHeaderCell.swift
//  MediCall
//
//  Created by Muhammad Azher on 10/09/2018.
//  Copyright © 2018 Salman Saeed. All rights reserved.
//

import UIKit

class TimingHeaderCell: UITableViewCell {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerImage: UIImageView!
    
    
    // MARK: - Life Cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}

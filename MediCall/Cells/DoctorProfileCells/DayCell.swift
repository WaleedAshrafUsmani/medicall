

import UIKit

class DayCell: UITableViewCell {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var addRemoveSlot: UIButton!
    
    
    // MARK: - Life Cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}


import UIKit

class DiscountCell: UITableViewCell {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    @IBOutlet weak var hasDiscount: UISwitch!
    
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}


import UIKit

class WorkPlaceCell: UITableViewCell {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    @IBOutlet weak var removedetails: UIButton!
    
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}

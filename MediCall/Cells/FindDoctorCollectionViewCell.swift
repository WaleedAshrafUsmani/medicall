

import UIKit

class FindDoctorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellContentView: UIView!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var findDoctorImage: UIImageView!
}

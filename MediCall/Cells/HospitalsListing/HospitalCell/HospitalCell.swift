
import UIKit
import Cosmos

class HospitalCell: UITableViewCell {

    @IBOutlet weak var ratings: CosmosView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var distanceLabelView: UIView!
    @IBOutlet weak var doctorPanel: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var nameOfHospital: UILabel!
    @IBOutlet weak var hospitalImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

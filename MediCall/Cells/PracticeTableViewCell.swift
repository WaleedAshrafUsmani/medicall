
import UIKit

class PracticeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var insideTableView: UITableView!
    @IBOutlet weak var deletePracticeButton: UIButton!
    @IBOutlet weak var newWeekLbl: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var addNewWeek: UIButton!
    
    @IBOutlet weak var discountView: UIView!
    
    @IBOutlet weak var disCountBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension PracticeTableViewCell {
    func setTableViewDataSourceDelegate<D:UITableViewDelegate & UITableViewDataSource>
        (_ dataSourceDelegate:D,forRow row:Int)
    {
        insideTableView.delegate = dataSourceDelegate
        insideTableView.dataSource = dataSourceDelegate
        
        insideTableView.reloadData()
    }
    
    
}






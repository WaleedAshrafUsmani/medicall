
import UIKit

class SpecialityTableViewCell: UITableViewCell {

    @IBOutlet weak var specialityLbl: UILabel!
    @IBOutlet weak var specialityImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

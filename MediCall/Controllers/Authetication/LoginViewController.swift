
import UIKit

class LoginViewController: MCOverlayViewController {

   
    @IBOutlet weak var buttonview: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var blurview: UIVisualEffectView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet var mainView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       mainView.setGradientBackground(colorOne: Colors.first, colorTwo: Colors.second, colorThree: Colors.third)
        mainView.insertSubview(blurview, aboveSubview: mainView)
        mainView.insertSubview(lineView, aboveSubview: mainView)
        mainView.insertSubview(buttonview, aboveSubview: mainView)
    }
}



import UIKit
import Alamofire


class MCRegisterAsNonMedicViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var FemaleRadioImage: UIImageView!
    @IBOutlet weak var maleRadioImage: UIImageView!
    @IBOutlet weak var malepopUpButton: UIButton!
    @IBOutlet weak var femalePopUpButton: UIButton!
    @IBOutlet weak var bloodGroupTextField: UITextField!
    @IBOutlet weak var cityLbl: UILabel!
    
    var statusId = 1
    var gender = ""
    let picker = UIDatePicker()
    let listPicker = UIPickerView()
    var userAuth = 0
    var validUser:UserValidation?
    var signUp = RegisterDoctor()
    var cityofId = 0
    
    var bloodGroups = ["A+","A-","B+","B-","O+","O-","AB+","AB-"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Register As Non-Medic"
        cityLbl.text = "Lahore"
        bloodGroupTextField.inputView = listPicker
        listPicker.delegate = self
        listPicker.dataSource = self
        //phoneTextField.delegate = self
        userNameTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
        phoneTextField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for:UIControlEvents.editingChanged)
        
        malepopUpButton.addTarget(self, action: #selector(handleCheck(_:)), for: .touchUpInside)
        femalePopUpButton.addTarget(self, action: #selector(handleCheck(_:)), for: .touchUpInside)
        self.view.addTapToDismiss()
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        validateUserFromApi(textField)
    }
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        if textField.text?.first != "3"
        {
            let myAlert = UIAlertController(title: "Warning!", message: " Phone number must start from three", preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
            { action in
                self.phoneTextField.text = ""
                //self.dismiss(animated: true , completion: nil);
                
            }
            myAlert.addAction(okAction);
            phoneTextField.text = ""
            
            self.present(myAlert, animated: true, completion: nil)
            
        }
        else {
            return
        }
        
    }
    
    
    
    func showError() {
        if validUser?.Error == true {
            ToastManager.showErrorMessage(message:(validUser?.errorMessage!)!)
        }
    }
    
    func validateUserFromApi(_ textfield:UITextField) {
        let username = textfield.text
        let apiCall = ApiManager.shared.isUserValid(userName: username!)
        LoaderManager.show(self.view)
        apiCall.then {
            valid -> Void in
            LoaderManager.hide(self.view)
            if valid.Error == true {
                self.validUser = valid
                self.showError()
            }
            
            }.catch {
                error -> Void in
        }
    }
    
    @IBAction func signUPBtnAction(_ sender: UIButton) {
        if userNameTextfield.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: "please enter user name")
        }
        else if fullNameTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: "please enter fullName")
        }
        else if phoneTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: "please enter Phone#")
        }
            
        else if phoneTextField.text?.count != 10 {
            ToastManager.showErrorMessage(message: "please enter valid number")
        }
        else if emailTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: "please enter Email")
        }
            
        else if emailTextField.text?.isValidEmail() != true{
            ToastManager.showErrorMessage(message: "Enter Valid Email")
        }
        else if passwordTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: "please enter password")
        }
        else if confirmPasswordTextField.text?.isEmpty ?? true
        {
            ToastManager.showErrorMessage(message: "please enter Repassword")
        }
        else if passwordTextField.text != confirmPasswordTextField.text {
            ToastManager.showErrorMessage(message: "Password doesn't match")
        
        }else if bloodGroupTextField.text!.isEmpty {
            ToastManager.showErrorMessage(message: "please add your blood group")
        }
        else {
            
            let bloodIndex = (bloodGroups.index(of: bloodGroupTextField.text!)! + 1)
            let phoneNumber = "92\(phoneTextField.text!)"
            
            let apiCall = ApiManager.shared.registerationNonMedic(userName: userNameTextfield.text!, fullName: fullNameTextField.text!,
                                                    phoneNo: phoneNumber,
                                                    City: cityofId,
                                                    Gender: gender,
                                                    bloodGroup: bloodIndex,
                                                    Email: emailTextField.text!,
                                                    Password: passwordTextField.text!)
            
            LoaderManager.show(self.view)
            apiCall.then {
                signUp -> Void in
                LoaderManager.hide(self.view)
                ToastManager.showErrorMessage(message: signUp.errorMessage!)
                self.signUp = signUp
            }
        }
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let ctrl = segue.destination as? VerifyStatusViewController {
            ctrl.regDoctor = signUp
        }
    }
    
    
    @IBAction func cityButtonAction(_ sender: UIButton) {
        //        let popupViewControllerIdentifier = "popupViewController"
        //        let main = UIStoryboard(name: "Main", bundle: nil)
        //        let controller = main.instantiateViewController(withIdentifier: "popUP") as! PopUpViewController
        //        controller.modalTransitionStyle = .crossDissolve
        //        controller.modalPresentationStyle = .overFullScreen
        //        print("clicked")
        //        self.present(controller, animated: false, completion: nil)
    }
    @objc func handleCheck(_ sender:UIButton) {
        if sender.tag == 0 {
            maleRadioImage.image = #imageLiteral(resourceName: "radio_btn_fill")
            FemaleRadioImage.image = #imageLiteral(resourceName: "radio_btn_unfill")
            gender = "Male"
            print("Male")
            
        }
        if sender.tag == 1 {
            FemaleRadioImage.image = #imageLiteral(resourceName: "radio_btn_fill")
            maleRadioImage.image = #imageLiteral(resourceName: "radio_btn_unfill")
            gender = "Female"
            print("Female")
        }
    }
    
    
    func requestWith(endUrl: String, parameters: [String : Any],imageUrl:URL){
        
        let url = endUrl    /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
                for (key, value) in parameters {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                
                
                // multipartFormData.append(imageUrl, withName: "image", fileName: "", mimeType: "image/png")
                //multipartFormData.append(imageUrl, withName: "Some File")
                //multipartFormData.append(fileURL: imageUrl, name: "photo")
                multipartFormData.append(imageUrl, withName: "some")
                
                
                
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response.result.value)
                    print(response.result.description)
                    
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }
    
    @objc func donePressed(){
        self.view.endEditing(true)
    }
    
    func listpicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([done], animated: false)
        
        //statusTextField.inputAccessoryView = toolBar
        bloodGroupTextField.inputView = listPicker
        
        
        
        //        listPicker = .date
        //        listPicker.maximumDate = Date()
        
        picker.setValue(UIColor.black, forKeyPath: "textColor")
        
    }
    
    
    @IBAction func citySelectionBtnAction(_ sender: UIButton) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let selectionVC = main.instantiateViewController(withIdentifier: "popUP") as? PopUpViewController
        selectionVC?.citySelectionDelegate = self as? CitySelectionDelegate
        present(selectionVC!, animated: true, completion: nil)
        
    }
}

extension MCRegisterAsNonMedicViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return bloodGroups[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bloodGroups.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        bloodGroupTextField.text = bloodGroups[row]
        statusId += row
        
    }
}

extension MCRegisterAsNonMedicViewController:CitySelectionDelegate {
    func didSelectCity(city: String?, cityId: Int?) {
        cityLbl.text = city
        cityofId = cityId!
    }
}

//extension UserFormViewController:UITextFieldDelegate {
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if phoneTextField.text?.first?.description == "3"
//        {
//
//            guard let text = phoneTextField.text else { return true }
//            let newLength = text.count + string.count - range.length
//            return newLength <= 11
//
//        }
//        else
//        {
//            let myAlert = UIAlertController(title: "Warning!", message: " Phone number must start from zero", preferredStyle: UIAlertControllerStyle.alert)
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//            { action in
//                self.phoneTextField.text = ""
//                //self.dismiss(animated: true , completion: nil);
//
//            }
//            myAlert.addAction(okAction);
//            phoneTextField.text = ""
//
//            self.present(myAlert, animated: true, completion: nil)
//            return true
//        }
//
//        }






//}












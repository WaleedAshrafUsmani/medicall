
import UIKit

class SignUpTypeViewController: MCOverlayViewController {

    @IBOutlet weak var registerAsMedicButton: UIImageView!
    @IBOutlet weak var registerAsNonMedicButton: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()  
        
        // Do any additional setup after loading the view.
        let medictap = UITapGestureRecognizer(target: self, action: #selector(registerAsMedicButtonTapped(tapGestureRecognizer:)))
        let nonMedictap = UITapGestureRecognizer(target: self, action: #selector(registerAsNonMedicButtonTapped(tapGestureRecognizer:)))
        registerAsMedicButton.isUserInteractionEnabled = true
        registerAsMedicButton.addGestureRecognizer(medictap)
        
        registerAsNonMedicButton.isUserInteractionEnabled = true
        registerAsNonMedicButton.addGestureRecognizer(nonMedictap)
    }
    
    @objc func registerAsMedicButtonTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.performSegue(withIdentifier: "registerAsMedic", sender: self)
    }
    
    @objc func registerAsNonMedicButtonTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        self.performSegue(withIdentifier: "registerAsNonMedic", sender: self)
    }
    
    @IBAction func pharmacyButtonPressed(_ sender: Any) {
        self.showComingSoonToast()
    }
    
    @IBAction func ambulanceButtonPressed(_ sender: Any) {
        self.showComingSoonToast()
    }
    
    @IBAction func hospitalButtonPressed(_ sender: Any) {
        self.showComingSoonToast()
    }
    
    @IBAction func labButtonPressed(_ sender: Any) {
        self.showComingSoonToast()
    }
    
    func showComingSoonToast() {
        ToastManager.showInfoMessage(message: SIGNUP.comingsoon)
    }
    
}

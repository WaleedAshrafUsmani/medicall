
import UIKit
import SnapKit

class BasicInfoViewController: UIViewController {

    
    
    @IBOutlet weak var SpecialityStackView: UIStackView!
    @IBOutlet weak var specialitySecondStack: UIStackView!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var someBtn: ButtonWithImage!
    
    
    @IBOutlet weak var servicesStackView: UIStackView!
    @IBOutlet weak var subSpecialityStackView: UIStackView!
    
    @IBOutlet weak var qualificationStackView: UIStackView!
    
    
    
    private var labels = [UILabel]()
    private var SpecialtiyArr = [String]()
    var buttons = [UIButton]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //SpecialityStackView.isHidden = true
        subSpecialityStackView.isHidden = true
        servicesStackView.isHidden = true
        qualificationStackView.isHidden = true
        
        //someBtn.isHidden = true
        
        
        
        
        
        
//       let new = UIButton()
//        newButton(new)
//        let second = UIButton()
//        newButton(second)
        
        
        
        
        
       // specialitySecondStack.isHidden = true
        
        //specialityView.isHidden = true
        
        //newView()
       // secondView()
        
//        specialityView.isHidden = true
//        specialitySecondView.isHidden = true
        
//        let bb = UILabel()
//        bb.text = "aflo"
//        let gg = UILabel()
//        gg.text = "Atif"
//        let first = UILabel()
//        first.text = "Some"
//        let second = UILabel()
//        second.text = "Vdo"
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//       // let som
//       // labels.append(UILabel)
//        SpecialityStackView.addArrangedSubview(first)
//        //SpecialityStackView.addArrangedSubview(second)
//        specialitySecondStack.addArrangedSubview(gg)
//       // specialitySecondStack.addArrangedSubview(bb)
        
        
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        let some2  = ButtonWithImage()
//        some2.setTitle("dermatology", for: UIControlState.normal)
//        secondButton(some2)
        
        for name in SpecialtiyArr
            
        {
            let some = ButtonWithImage()
            some.setTitle(name, for: UIControlState.normal)
            //newButton(some)
            
            
            
            if SpecialtiyArr.index(of: name)!%2 == 0 {
                newButton(some)
            }
            else {
                secondButton(some)
            }
            
        }
    }
    
    func newButton(_ button:UIButton) {
        //let newbutton = button
        button.setCornerRadius(r:15)
        button.setImage(#imageLiteral(resourceName: "cross"), for: UIControlState.normal)
       // button = ButtonWithImage()
        button.backgroundColor = Colors.darkBlue
        SpecialityStackView.addArrangedSubview(button)
        button.snp.makeConstraints { (make) in
            make.height.equalTo(30)
           // make.width.equalTo(150)
        }
    }
    
    func secondButton(_ button:UIButton) {
        //let newbutton = button
        button.setCornerRadius(r:15)
        button.setImage(#imageLiteral(resourceName: "cross"), for: UIControlState.normal)
        // button = ButtonWithImage()
        button.backgroundColor = Colors.darkBlue
        specialitySecondStack.addArrangedSubview(button)
        button.snp.makeConstraints { (make) in
            make.height.equalTo(30)
           // make.width.equalTo(150)
        }
    }
    
    
    func newView() {
        let speView = UIView(frame:CGRect(x: 0, y: 0, width: 150, height: 30))
        speView.backgroundColor = Colors.darkBlue
        SpecialityStackView.addArrangedSubview(speView)
        
        speView.snp.makeConstraints { (make) in
            //make.height.equalTo(30)
            //make.width.equalTo(150)
        }
        
        let label1 = UILabel()
        label1.text = "Child Special"
        label1.textColor = Colors.white
        speView.addSubview(label1)
        label1.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(5)
            make.top.equalToSuperview().offset(5)
            
        }
        
        let image1 = UIImageView()
        image1.image  = #imageLiteral(resourceName: "cross")
        
        speView.addSubview(image1)
        image1.snp.makeConstraints { (make) in
            make.height.width.equalTo(15)
            make.trailing.equalToSuperview().offset(-5)
            make.top.equalToSuperview().offset(5)
        }
    }
    
    func secondView() {
        
        let sperView = UIView(frame:CGRect(x: 0, y: 40, width: 150, height: 30))
        sperView.backgroundColor = Colors.darkBlue
        SpecialityStackView.axis = .vertical
        SpecialityStackView.spacing = 10
        SpecialityStackView.addArrangedSubview(sperView)
        
        
        sperView.snp.makeConstraints { (make) in
            //make.height.equalTo(30)
            //make.width.equalTo(150)
        }
        
        let label2 = UILabel()
        label2.text = "Child Special"
        label2.textColor = Colors.white
        sperView.addSubview(label2)
        label2.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(5)
            make.top.equalToSuperview().offset(5)
            
        }
        
        let image2 = UIImageView()
        image2.image  = #imageLiteral(resourceName: "cross")
        
        sperView.addSubview(image2)
        image2.snp.makeConstraints { (make) in
            make.height.width.equalTo(15)
            make.trailing.equalToSuperview().offset(-5)
            make.top.equalToSuperview().offset(5)
        }
        
    }
   

    
    @IBAction func specialityBtnAction(_ sender: UIButton) {
        let main = UIStoryboard(name: "LoginStoryboard", bundle: nil)
        let selectionVC = main.instantiateViewController(withIdentifier: "speciality") as? SpecailityViewController
        //selectionVC?.citySelectionDelegate = self as? CitySelectionDelegate
        selectionVC?.specialitySelectionDelegate = self
        present(selectionVC!, animated: true, completion: nil)
    }
    

}

extension BasicInfoViewController:SpecialitySelectionDelegate {
    func didSelectSpeciality(speciality: [String]?) {
        SpecialtiyArr = speciality!
    }
    
    
    
    
}







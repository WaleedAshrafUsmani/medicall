

import UIKit
import SwiftyJSON
import Alamofire

class BioViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var doctorDetail = SingleDoctor()
    
    @IBOutlet weak var drProfileImageView: UIImageView!
    
    @IBOutlet weak var drExperience: UILabel!
    
    @IBOutlet weak var drDistance: UILabel!
    
    @IBOutlet weak var drNameLbl: UILabel!
    @IBOutlet weak var qualificationLabel: UILabel!
    @IBOutlet weak var discountImage: UIImageView!
    
    @IBOutlet weak var feeLbl: UILabel!
    @IBOutlet weak var drSpecialityLbl: UILabel!
    @IBOutlet weak var bioTableView: UITableView!
    var aboutme = String()
    var photoGallery = [String]()
    var qualification = [String]()
    var registeration = [String]()
    var institutions = [String]()
    var expertise = [String]()
    var achivements = String()
    var publication = String()
    var extracurricular = String()
    
    var names = ["About Me","Photo Gallery","Qualification","Registraions","institutions","Expertise","Achievments","Publications","Other Extracurriculae Activities"]
    var one = ["this is some data","asdfasfdasdf","adfasfasfafs"]
    var two = ["this is some data","asdfasfdasdf","adfasfasfafs","adsasdasdasdasdasdasdasd"]
    var three = ["asdasd","asdasdasd"]
    
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return names.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        return 75
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if section == 0 {
            rowCount = aboutme.count
        }
        if section == 1 {
            rowCount = qualification.count
        }
        
        if section == 2 {
            rowCount = registeration.count
        }
        if section == 3 {
            rowCount = institutions.count
        }
        if section == 4 {
            rowCount = expertise.count
        }
        if section == 5 {
            rowCount = achivements.count
        }
        
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "some", for: indexPath) as! BioTableViewCell
        if indexPath.section == 0
        {
           cell.aboutmeTextView.text = aboutme
            return cell
        }
        if indexPath.section == 1
        {
            cell.aboutmeTextView.text = qualification[indexPath.row]
            return cell
        }
        if indexPath.section == 2
        {
            cell.aboutmeTextView.text = registeration[indexPath.row]
            return cell
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 1 {
            return 0
        }
        return 30
        
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTableViewCell
        header.myImage.image = #imageLiteral(resourceName: "doctor")
        header.myLbl.text = names[section]
        header.contentView.backgroundColor = UIColor(0xEAEAEA)
        return header.contentView
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        dataFromApi()
        
        //print(doctorDetail.doctors?.doctor_about_me ?? "somevalue of Json")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   func dataFromApi ()
   {
           let url = "http://themedicall.com/medi-app-api/get-doctor-bio-details?key=Vx0cbjkzfQpyTObY8vfqgN1us&doctor_id=13607"
    
                Alamofire.request(url,method:.post).responseJSON
                    {
                    response in
                    switch (response.result)
                    {
                    case .success(let value):
                        
                   let swiftyJsonVar = JSON(value)
                   
                   
                   self.aboutme = swiftyJsonVar["doctors"]["doctor_about_me"].stringValue
                   self.bioTableView.reloadData()
                   for(_,images) in swiftyJsonVar["doctors"]["gallery"] {
                    let photo = images["doctor_gallery_img"].stringValue
                    self.photoGallery.append(photo)
                    self.bioTableView.reloadData()
                   }
                   
                   
                   
                   let qualifi = swiftyJsonVar["doctors"]["qualifications"]
                   for(_,object) in qualifi
                   {
                    let name = object["qualifications"]["qualification_title"].stringValue
                    
                    self.qualification.append(name)
                    self.bioTableView.reloadData()
                   }
                   
                     
                   let insti = swiftyJsonVar["doctors"]["institutions"]
                   for(_,object) in insti
                   {
                    let name = object["institutions"]["institutions_doctor_title"].stringValue
                    
                    self.institutions.append(name)
                    self.bioTableView.reloadData()
                        }
                  
                   let experti = swiftyJsonVar["doctors"]["expertise"]
                   for(_,object) in experti
                   {
                    let name = object["expertise"]["service_title"].stringValue
                    
                    self.expertise.append(name)
                    self.bioTableView.reloadData()
                        }
                    
                    case .failure(let error):
                    print(error)
                        
    }
    
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
}


import UIKit

class BioVC: UIViewController {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var doctorDetails : Doctors!
    var doctor : AllDoctor?
    
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var tableView: UITableView!
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfiguration()
        // Do any additional setup after loading the view.
        let params =
            [
                "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
                "doctor_id": doctorDetails!.doctor_id!,
                "uniqueId": UIDevice.current.identifierForVendor!.uuidString
                ] as [String : Any]
        
        ApiManager.shared.tryFetchDoctorBioDetails(parameters: params, success: { [weak self] (doctorResponse) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.doctor = doctorResponse
//            weakSelf.doctorDetails = doctorResponse!.doctor!
//            weakSelf.tableView.reloadData()
            
        }) { (error) in
            print(error)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    func viewConfiguration() {
        
        let timingCell = UINib.init(nibName: "TimingCell", bundle: Bundle.main)
        let locationCell = UINib.init(nibName: "LocationsCell", bundle: Bundle.main)
        let aboutMe = UINib.init(nibName: "AboutMeCell", bundle: Bundle.main)
        let timingHeaderCell = UINib.init(nibName: "TimingHeaderCell", bundle: Bundle.main)
        let practiceFooterCell = UINib.init(nibName: "PracticeFooterCell", bundle: Bundle.main)
        tableView.register(timingCell, forCellReuseIdentifier: "TimingCell")
        tableView.register(locationCell, forCellReuseIdentifier: "LocationsCell")
        tableView.register(aboutMe, forCellReuseIdentifier: "AboutMeCell")
        
        tableView.register(timingHeaderCell, forCellReuseIdentifier: "TimingHeaderCell")
        tableView.register(practiceFooterCell, forCellReuseIdentifier: "PracticeFooterCell")
        
        
        
    }
    
    // MARK: - IBActions
    

}









extension BioVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell : AboutMeCell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell") as! AboutMeCell
        if doctor != nil {
            
            if indexPath.section == 0 {
                cell.aboutMe.text = doctor!.doctor!.doctor_about_me!
            }
            else if indexPath.section == 1 {
                var qualificationString = ""
                for qualification in doctor!.doctor!.qualifications! {
                    
                    qualificationString = qualificationString + "\u{2022}" + qualification.qualifications!.qualification_title! + "\n"
                    
                }
                qualificationString.removeLast()
                cell.aboutMe.text = qualificationString
            }
            else if indexPath.section == 2 {
                cell.aboutMe.text = "PMDC"
                
            }
        }
        
        else {
            
            return UITableViewCell()
        }
//        cell.aboutMe.text = self.doctor!.doctor!.
        return cell
        
        
    }
    
    
}

extension BioVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "About Me"
            cell.headerTitle.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        }
        else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Qualification"
            cell.headerTitle.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Registrations"
            cell.headerTitle.font = UIFont.boldSystemFont(ofSize: 17)
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
}

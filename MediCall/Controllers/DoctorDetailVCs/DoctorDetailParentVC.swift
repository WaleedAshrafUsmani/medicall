
import UIKit
import Parchment

class DoctorDetailParentVC: UIViewController {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var doctorDetails : Doctors!
    
    
    var pagingController: FixedPagingViewController!
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var doctorImage: UIImageView!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var verificationImageView: UIImageView!
    @IBOutlet weak var qualification: UILabel!
    @IBOutlet weak var doctorFee: UILabel!
    @IBOutlet weak var totalExperience: UILabel!
    @IBOutlet weak var ratings: UILabel!
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Doctor Details"
        print("Doctor details are \(self.doctorDetails!)")
        configueDoctorView()
        
        // Do any additional setup after loading the view.
        
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
        
        let practiceVC = storyBoard.instantiateViewController(withIdentifier: "PracticeDetailsVC") as! PracticeVC
        practiceVC.doctorDetails = self.doctorDetails!
        let bioVC = storyBoard.instantiateViewController(withIdentifier: "BioVC") as! BioVC
        bioVC.doctorDetails = self.doctorDetails!
        let reviewsVC = storyBoard.instantiateViewController(withIdentifier: "ReviewsVC")
        let packagesVC = storyBoard.instantiateViewController(withIdentifier: "PackagesVC")
        let answersVC = storyBoard.instantiateViewController(withIdentifier: "AnswersVC")
        let blogVC = storyBoard.instantiateViewController(withIdentifier: "BlogVC")
        
        
        
//        let secondViewController = storyboard.instantiateViewController(withIdentifier: "GroceriesVC") as! GroceriesVC
        
        practiceVC.title = "Practice"
        bioVC.title = "Bio"
        reviewsVC.title = "Reviews"
        packagesVC.title = "Packages"
        answersVC.title = "Answers"
        blogVC.title = "Blog"
//        firstViewController.index = 0
        
        pagingController = DoctorDetailParentVC.configueTopTabBar(with: [practiceVC,bioVC,reviewsVC,packagesVC,answersVC,blogVC], in: self)
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.pagingController!.collectionView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    func configueDoctorView() {
        
        let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + doctorDetails!.doctor_img!
        
        doctorImage.af_setImage(withURL: URL(string: imageUrl)!)
        doctorImage.roundImage()
        
        doctorName.text = doctorDetails!.doctor_full_name!
        
        if doctorDetails!.doctor_verified_status! == 0 {
            verificationImageView.isHidden = true
        }
        else {
            verificationImageView.isHidden = false
        }
        var allQualifications = ""
        
        for qualification in doctorDetails!.qualifications! {
            
            allQualifications.append(qualification.qualifications!.qualification_title!)
            allQualifications.append(",")
        }
        if allQualifications.last! == "," {
            allQualifications.removeLast()
        }
        
        qualification.text = allQualifications
        
        if doctorDetails.docor_max_fee != nil && doctorDetails.doctor_min_fee != nil {
            
            doctorFee.text = "Fee: " + "\(doctorDetails.docor_max_fee!)" + "-" + "\(doctorDetails.doctor_min_fee!)"
            
        }
        else if doctorDetails.doctor_min_fee == nil {
            doctorFee.text = "Fee: \(doctorDetails.docor_max_fee!)"
        }
        else if doctorDetails.docor_max_fee == nil {
            doctorFee.text = "Fee: \(doctorDetails.doctor_min_fee!)"
        }
        else {
            doctorFee.text = "Fee: 0"
        }
        
        totalExperience.text = doctorDetails.doctor_experience!
//        ratings.text = doctorDetails.ratings![0]
        
    }
    
    
    // MARK: - IBActions

}





extension DoctorDetailParentVC {
    
    class func configueTopTabBar(with viewControllers : [UIViewController],in viewController : UIViewController) -> FixedPagingViewController
    {
        let view = viewController.view!
        let pagingViewController = FixedPagingViewController(viewControllers: viewControllers)
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        
        viewController.addChildViewController(pagingViewController)
        
        view.addSubview(pagingViewController.view)
        
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.collectionView.bounces = false
        pagingViewController.options.textColor = .white
        pagingViewController.options.selectedTextColor = .white
        pagingViewController.indicatorColor = .white
        pagingViewController.options.borderColor = .white
        pagingViewController.options.selectedTextColor = .white
//        pagingViewController.options.menuHeight = 20
        
        
        
        
        
        
        view.constrainToEdges(pagingViewController.view)
        pagingViewController.indicatorOptions = .visible(height: 3, zIndex: 0, spacing: UIEdgeInsets.zero, insets: UIEdgeInsets.zero)
        pagingViewController.options.menuInsets = UIEdgeInsets.init(top: -5, left: 0, bottom: 0, right: 0)
        pagingViewController.options.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: pagingViewController.options.menuItemSize.width - 40, height: pagingViewController.options.menuItemSize.height - 10)
        pagingViewController.options.menuItemSpacing = 0
        pagingViewController.options.includeSafeAreaInsets = false
        pagingViewController.options.borderColor = .clear
        
        
        pagingViewController.options.backgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        pagingViewController.options.selectedBackgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        
        pagingViewController.didMove(toParentViewController: viewController)
        
        return pagingViewController
        
    }
    
}

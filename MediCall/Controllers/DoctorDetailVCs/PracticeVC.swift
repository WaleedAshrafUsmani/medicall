
import UIKit
import GoogleMaps
import CoreLocation

class PracticeVC: UIViewController {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var doctorDetails : Doctors!
    
    
    let timingSection = 0
    let locationSection = 1
    let servicesSection = 2
    
    
    var isFirstSectionExpanded = false
    var isThirdSectionExpanded = false
    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        viewConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let params =
            [
                "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
                "doctor_id": doctorDetails!.doctor_id!,
                "uniqueId": UIDevice.current.identifierForVendor!.uuidString
                ] as [String : Any]
        
        
        ApiManager.shared.tryFetchDoctorPracticeDetails(parameters: params, success: { [weak self] (doctorResponse) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.doctorDetails = doctorResponse!.doctor!
            weakSelf.tableView.reloadData()
            
        }) { (error) in
            print(error)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    func viewConfiguration() {
        
        let timingCell = UINib.init(nibName: "TimingCell", bundle: Bundle.main)
        let locationCell = UINib.init(nibName: "LocationsCell", bundle: Bundle.main)
        let timingHeaderCell = UINib.init(nibName: "TimingHeaderCell", bundle: Bundle.main)
        let practiceFooterCell = UINib.init(nibName: "PracticeFooterCell", bundle: Bundle.main)
        tableView.register(timingCell, forCellReuseIdentifier: "TimingCell")
        tableView.register(locationCell, forCellReuseIdentifier: "LocationsCell")
        
        tableView.register(timingHeaderCell, forCellReuseIdentifier: "TimingHeaderCell")
        tableView.register(practiceFooterCell, forCellReuseIdentifier: "PracticeFooterCell")
        
        
        
    }
    
    
    // MARK: - IBActions

}









extension PracticeVC : UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == timingSection {
            if doctorDetails.hospitals![0].hospitals!.practices != nil {
                return doctorDetails.hospitals![0].hospitals!.practices!.count
            }
            
            return 0
        }
        else if section == locationSection {
            return 1
        }
        else if section == servicesSection {
            return 10
        }
        else {
            return 0
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == timingSection {
            let cell : TimingCell = tableView.dequeueReusableCell(withIdentifier: "TimingCell") as! TimingCell
            
            if doctorDetails.hospitals![0].hospitals!.practices != nil {
                let practiceData = doctorDetails.hospitals![0].hospitals!.practices![indexPath.row]
                cell.weekDay.text = practiceData.week_days!.week_day_title!
                cell.timings.text = practiceData.doctor_timing_start_time!.convert24HourTo12HourTime() +  " - " + practiceData.doctor_timing_end_time!.convert24HourTo12HourTime()
            }
            
            
            
            
            
            return cell
        }
        else if indexPath.section == servicesSection {
            let cell : TimingCell = tableView.dequeueReusableCell(withIdentifier: "TimingCell") as! TimingCell
            
            if doctorDetails.services != nil {
                let practiceData = doctorDetails.services![indexPath.row].services!
                cell.weekDay.text = practiceData.service_title!
                cell.timings.text = practiceData.service_title!
            }
            return cell
        }
        else if indexPath.section == locationSection {
            
            let cell : LocationsCell = tableView.dequeueReusableCell(withIdentifier: "LocationsCell") as! LocationsCell
            
            if doctorDetails.hospitals != nil {
                for tmpHospital in doctorDetails.hospitals! {
                    let latitude = tmpHospital.hospitals!.hospital_lat!
                    let longitude = tmpHospital.hospitals!.hospital_lng!
                    let position = CLLocationCoordinate2D(latitude: latitude.doubleValue()!, longitude: longitude.doubleValue()!)
                    let marker = GMSMarker(position: position)
                    marker.title = tmpHospital.hospitals!.hospital_name!
                    marker.map = cell.mapView
                }
                let camera = GMSCameraPosition.camera(withLatitude: doctorDetails.hospitals![0].hospitals!.hospital_lat!.doubleValue()!, longitude: doctorDetails.hospitals![0].hospitals!.hospital_lng!.doubleValue()!, zoom: 16)
                cell.mapView.camera = camera
            }
            
            
            
            return cell
            
        }
        return UITableViewCell()
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == timingSection {
            
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            }
            else {
                if isFirstSectionExpanded == true {
                    return UITableViewAutomaticDimension
                }
                else {
                    return 0
                }
            }
        }
        else if indexPath.section == locationSection{
            return 200
        }
        else if indexPath.section == servicesSection {
            
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            }
            else {
                if isThirdSectionExpanded == true {
                    return UITableViewAutomaticDimension
                }
                else {
                    return 0
                }
            }
        }
        else {
            return 0
        }
    }
    
    
}





extension PracticeVC : UITableViewDelegate {


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == timingSection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Timings :"
            return cell
        }
        else if section == servicesSection {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Services :"
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Location :"
            return cell
        }
    
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        
        if section == timingSection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeFooterCell") as! PracticeFooterCell
            
            if isFirstSectionExpanded {
                
                cell.viewDetailsButton.setTitle("View Less Timings", for: .normal)
            }
            else {
                cell.viewDetailsButton.setTitle("View All Timings", for: .normal)
            }
            
            cell.viewDetailsButton.addTarget(self, action: #selector(PracticeVC.showHideTimingList(_:)), for: .touchDown)
            return cell
        }
        else if section == servicesSection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeFooterCell") as! PracticeFooterCell
            
            if isThirdSectionExpanded {
                
                cell.viewDetailsButton.setTitle("View Less Services", for: .normal)
            }
            else {
                cell.viewDetailsButton.setTitle("View All Services", for: .normal)
            }
            
            cell.viewDetailsButton.addTarget(self, action: #selector(PracticeVC.showHideServicesList(_:)), for: .touchDown)
            return cell
        }
        else {
            return nil
        }
        
    }
    @objc func showHideTimingList(_ sender : UIButton) {
        
        
        tableView.beginUpdates()
        isFirstSectionExpanded = !isFirstSectionExpanded
        tableView.endUpdates()
        tableView.reloadData()
        
    }
    @objc func showHideServicesList(_ sender : UIButton) {
        
        tableView.beginUpdates()
        isThirdSectionExpanded = !isThirdSectionExpanded
        tableView.endUpdates()
        tableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }


}

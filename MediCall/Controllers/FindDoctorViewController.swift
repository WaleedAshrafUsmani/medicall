

import UIKit
import Alamofire
import  AlamofireImage
import CoreLocation

class FindDoctorViewController: UIViewController,CLLocationManagerDelegate {
    
    // MARK: - Class Properties
    
    var doctor = [DoctorType]()
    var dataSourceArray = [Specilatie]()
    var identity:Int = 0
    let defaults = UserDefaults.standard
    var cityArray = [LocalCities]()
    
    @IBOutlet weak var findDoctorCollectionView: UICollectionView!
    
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        // cityArray = DataManager.shared.getNotes()
        //print(cityArray[0].title)
        for citi in cityArray {
            print("gggggg")
        }
        //print(cityArray)
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"listingbackground")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        
        //        let imageBackground = UIColor(patternImage: UIImage(named: "listingbackground")!)
        self.findDoctorCollectionView.backgroundView = imageView
        
        
        LoaderManager.show(self.view)
        let ApiCall =  ApiManager.shared.fetchDataFromAPi()
        ApiCall.then {
            specilaities -> Void in
            self.dataSourceArray = specilaities
            LoaderManager.hide(self.view)
            self.findDoctorCollectionView.reloadData()
            }.catch {
                error -> Void in
                LoaderManager.hide(self.view)
                print("Server Error")
        }
        
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
    
    
    
    
    
    
    
}

    
    

    
       
    //@IBAction func apiResponse(_ sender: UIButton) {
       // guard let url = URL(string: "http://themedicall.com/medi-app-api/get-specialities/Vx0cbjkzfQpyTObY8vfqgN1us") else {  return }
        //let session = URLSession.shared
        //let task = session.dataTask(with: url) { (data, _, _) in
        // guard let data = data else {return print("no data")}
//        Alamofire.request(url).responseJSON { (response) in
//
//            let result = response.data
//
//            do {
//                let json = try JSONDecoder().decode([DoctorType].self, from: result!)
//                print(json)
//                self.doctor = json
//                DispatchQueue.main.async {
//                    self.findDoctorCollectionView.reloadData()
//                }
//
//            }
//
//            catch{}
//        }
   // }
    
extension FindDoctorViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "findDoctor", for: indexPath) as! FindDoctorCollectionViewCell
        cell.descriptionLbl.text =  dataSourceArray[indexPath.row].speciality_designation
        //cell.descriptionLbl.text = mainMenu[indexPath.row]
        let imageUrl = "http://themedicall.com/public/uploaded/img/specialities/" + dataSourceArray[indexPath.row].speciality_icon!
        
        let url = URL(string: imageUrl)
        cell.findDoctorImage.af_setImage(withURL: url!)
        cell.cellContentView.layer.shadowColor = UIColor.white.cgColor
        cell.cellContentView.layer.shadowOffset = CGSize(width: 0, height: 25.0)//CGSizeMake(0, 2.0);
        cell.cellContentView.layer.shadowRadius = 20.0
        cell.cellContentView.layer.shadowOpacity = 10.0
        cell.cellContentView.layer.masksToBounds = false
        
        return cell
    }
}
 

extension FindDoctorViewController : UICollectionViewDelegate {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        identity = dataSourceArray[indexPath.row].speciality_id!
        defaults.set(identity, forKey: "userId")
        self.performSegue(withIdentifier: "doctorDetail", sender: identity)
        
    }
    
}
    
extension FindDoctorViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CGFloat((collectionView.frame.size.width / 2) - 0.5), height: CGFloat(90))
    }
}


    
    

    

    

   
    

   



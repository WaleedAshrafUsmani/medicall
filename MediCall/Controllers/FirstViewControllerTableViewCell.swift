//
//  FirstViewControllerTableViewCell.swift
//  MediCall
//
//  Created by Salman Saeed on 07/03/2018.
//  Copyright © 2018 Salman Saeed. All rights reserved.
//

import UIKit

class FirstViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var cityTitleLabelCell: UILabel!
    
    @IBOutlet weak var cityIdLabelCell: UILabel!
    
    override func awakeFromNib() {
       // @IBOutlet weak var cityIdLabelCell: UILabel!
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


import UIKit
import AZTableView

class AllHospitalsListingVC: AZTableViewController {
    
    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var hospitals : [NestedHospitals]!
    var pageNo = 0
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"listingbackground")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        
        //        let imageBackground = UIColor(patternImage: UIImage(named: "listingbackground")!)
        self.tableView!.backgroundView = imageView
        
        fetchData()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let indexPath = sender as! IndexPath
        let destination = segue.destination as! HospitalDetailParentVC
        destination.hospital = self.hospitals[indexPath.row]
        
        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
}





extension AllHospitalsListingVC {
    
    override func AZtableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.hospitals == nil || self.hospitals.count == 0 {
            return 0
        }
        return 10
    }
    override func AZtableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.hospitals == nil || self.hospitals.count == 0 {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "HospitalCell") as! HospitalCell
        
        let tmpHospital = hospitals[indexPath.row]
        
        let imageUrl = "http://themedicall.com/public/uploaded/img/hospitals/" + hospitals[indexPath.row].hospital_img!
        
        let url = URL(string: imageUrl)
        cell.hospitalImage.af_setImage(withURL: url!)
        cell.hospitalImage.roundImage()
        cell.nameOfHospital.text = tmpHospital.hospital_name!
        cell.address.text = tmpHospital.hospital_addr!
        cell.doctorPanel.text = "Panel Doctor: " + "\(tmpHospital.hospital_panels!)"
        cell.ratings.rating = 0
        cell.distance.text = "5 KM"
        cell.collectionView.tag = indexPath.row
        cell.collectionView.reloadData()
        return cell
    }
}
extension AllHospitalsListingVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "hospitalDetail", sender: indexPath)
    }
    
}




extension AllHospitalsListingVC {
    
    func getDataFromServer() {
        
        ApiManager.shared.tryFetchAllHospital(id: 0, offset: self.pageNo, success: { [weak self] (hospitalsResponse) in
            
            guard let weakSelf = self else { return }
            LoaderManager.hide(weakSelf.view)
            if weakSelf.hospitals == nil || weakSelf.hospitals.count == 0 {
                weakSelf.hospitals = hospitalsResponse!.hospitals!
            }
            else {
                weakSelf.hospitals.append(contentsOf: hospitalsResponse!.hospitals!)
            }
            
            if weakSelf.hospitals.count < hospitalsResponse!.total! {
                weakSelf.pageNo = weakSelf.pageNo + 1
                weakSelf.didfetchData(resultCount: weakSelf.hospitals.count, haveMoreData: true)
            }
            else {
                weakSelf.didfetchData(resultCount: weakSelf.hospitals.count, haveMoreData: false)
            }
            
            
            
        }) { (error) in
            
        }
        
    }
    
}


extension AllHospitalsListingVC {
    
    
    
    override func fetchData() {
        super.fetchData()
        LoaderManager.show(self.view)
        if self.hospitals != nil {
            self.hospitals.removeAll()
        }
        
        self.tableView!.reloadData()
        pageNo = 1
        getDataFromServer()
    }
    override func fetchNextData() {
        super.fetchNextData()
        getDataFromServer()
    }
    
    
    
    
}


extension AllHospitalsListingVC : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let tag = collectionView.tag
        let tmpHospital = self.hospitals[tag]
        return tmpHospital.doctors!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorListInHospitalCell", for: indexPath) as! DoctorListInHospitalCell
        let tag = collectionView.tag
        let tmpHospital = self.hospitals[tag]
        let currentDoctor = tmpHospital.doctors![indexPath.item]
        cell.doctorName.text = currentDoctor.doctors!.doctor_full_name!
        
        let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + currentDoctor.doctors!.doctor_img!
        
        let url = URL(string: imageUrl)
        cell.doctorImage.af_setImage(withURL: url!)
        cell.doctorImage.roundImage()
        
        return cell
    }
    
}

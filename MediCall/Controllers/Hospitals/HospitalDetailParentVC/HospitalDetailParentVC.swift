
import UIKit
import Parchment
import Cosmos

class HospitalDetailParentVC: UIViewController {

    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var pagingController: FixedPagingViewController!
    
    var hospital : NestedHospitals!
    
    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var ratings: CosmosView!
    
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var distanceLabelView: UIView!
    @IBOutlet weak var doctorPanel: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var nameOfHospital: UILabel!
    @IBOutlet weak var hospitalImage: UIImageView!
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        let tmpHospital = hospital!
        
        let imageUrl = "http://themedicall.com/public/uploaded/img/hospitals/" + hospital.hospital_img!
        
        let url = URL(string: imageUrl)
        self.hospitalImage.af_setImage(withURL: url!)
        self.hospitalImage.roundImage()
        self.nameOfHospital.text = tmpHospital.hospital_name!
        self.address.text = tmpHospital.hospital_addr!
        self.doctorPanel.text = "Panel Doctor: " + "\(tmpHospital.hospital_panels!)"
        self.ratings.rating = 0
        self.distance.text = "5 KM"
        
        
        
        
        
        let storyBoard = UIStoryboard.init(name: "HospitalsStoryboard", bundle: nil)
        
        let hospitalDetailVC = storyBoard.instantiateViewController(withIdentifier: "HospitalInfoVC") as! HospitalInfoVC
        let panelDoctorsVC = storyBoard.instantiateViewController(withIdentifier: "PanelDoctorsVC") as! PanelDoctorsVC
        
        
        //
        //
        hospitalDetailVC.title = "Hospital Info"
        panelDoctorsVC.title = "Panel Doctors"
        
        pagingController = HospitalDetailParentVC.configueTopTabBar(with: [hospitalDetailVC,panelDoctorsVC], in: self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.pagingController!.collectionView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 30)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions

}





extension HospitalDetailParentVC {
    
    class func configueTopTabBar(with viewControllers : [UIViewController],in viewController : UIViewController) -> FixedPagingViewController
    {
        let view = viewController.view!
        let pagingViewController = FixedPagingViewController(viewControllers: viewControllers)
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        
        viewController.addChildViewController(pagingViewController)
        
        view.addSubview(pagingViewController.view)
        
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.collectionView.bounces = false
        pagingViewController.options.textColor = .white
        pagingViewController.options.selectedTextColor = .white
        pagingViewController.indicatorColor = .white
        pagingViewController.options.borderColor = .white
        pagingViewController.options.selectedTextColor = .white
        //        pagingViewController.options.menuHeight = 20
        
        
        
        
        
        
        view.constrainToEdges(pagingViewController.view, with: 185)
        pagingViewController.indicatorOptions = .visible(height: 3, zIndex: 0, spacing: UIEdgeInsets.zero, insets: UIEdgeInsets.zero)
        pagingViewController.options.menuInsets = UIEdgeInsets.init(top: -5, left: 0, bottom: 0, right: 0)
        pagingViewController.options.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: pagingViewController.options.menuItemSize.width - 40, height: pagingViewController.options.menuItemSize.height - 10)
        pagingViewController.options.menuItemSpacing = 0
        pagingViewController.options.includeSafeAreaInsets = false
        pagingViewController.options.borderColor = .clear
        
        pagingViewController.options.backgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        pagingViewController.options.selectedBackgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        
        pagingViewController.didMove(toParentViewController: viewController)
        
        return pagingViewController
        
    }
    
}

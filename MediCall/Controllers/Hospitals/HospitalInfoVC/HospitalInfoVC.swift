
import UIKit
import GoogleMaps
import CoreLocation

class HospitalInfoVC: UIViewController {
    
    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var doctorDetails : Doctors!
    
    
    let aboutSection = 0
    let affiliationSection = 1
    let timingSection = 2
    let spaceSection = 3
    let locationSection = 4
    let servicesSection = 5
    
    
    var isFirstSectionExpanded = false
    var isThirdSectionExpanded = false
    
    
    
    var hospitals : Hospital?
    
    
    // MARK: - IBOutlets
    
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        
        // Do any additional setup after loading the view.
        
        viewConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let params =
            [
                "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
                "hospital_id": 48,
                "uniqueId": UIDevice.current.identifierForVendor!.uuidString
                ] as [String : Any]
        
        
        ApiManager.shared.tryFetchHospitalDetails(parameters: params, success: { [weak self] (doctorResponse) in
            
            guard let weakSelf = self else { return }
            weakSelf.hospitals = doctorResponse
            weakSelf.tableView.reloadData()
            weakSelf.tableView.isHidden = false
            
        }) { (error) in
            print(error)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    
    func viewConfiguration() {
        
        let timingCell = UINib.init(nibName: "TimingCell", bundle: Bundle.main)
        let locationCell = UINib.init(nibName: "LocationsCell", bundle: Bundle.main)
        let timingHeaderCell = UINib.init(nibName: "TimingHeaderCell", bundle: Bundle.main)
        let practiceFooterCell = UINib.init(nibName: "PracticeFooterCell", bundle: Bundle.main)
        tableView.register(timingCell, forCellReuseIdentifier: "TimingCell")
        tableView.register(UINib.init(nibName: "AboutMeCell", bundle: Bundle.main), forCellReuseIdentifier: "AboutMeCell")
        
        tableView.register(locationCell, forCellReuseIdentifier: "LocationsCell")
        
        tableView.register(timingHeaderCell, forCellReuseIdentifier: "TimingHeaderCell")
        tableView.register(practiceFooterCell, forCellReuseIdentifier: "PracticeFooterCell")
        
        
        
    }
    
    
    // MARK: - IBActions
    
}









extension HospitalInfoVC : UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 5 {
            if doctorDetails == nil {
                return 0
            }
            if doctorDetails.hospitals![0].hospitals!.practices != nil {
                return doctorDetails.hospitals![0].hospitals!.practices!.count
            }
            
            return 0
        }
        else {
            return 1
        }
        
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if hospitals == nil {
            return UITableViewCell()
        }
        
        if indexPath.section == 0 {
            let cell : AboutMeCell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell") as! AboutMeCell
            
            cell.aboutMe.text = hospitals!.hospitalAboutUs!
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell : AboutMeCell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell") as! AboutMeCell
            
            cell.aboutMe.text = hospitals!.affiliations![0].affiliations!.affiliationTitle!
            return cell
        }
        else if indexPath.section == 2 {
            let cell : AboutMeCell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell") as! AboutMeCell
            
            cell.aboutMe.text = hospitals!.hospitalTimingStatus!
            return cell
        }
        else if indexPath.section == 3 {
            let cell : AboutMeCell = tableView.dequeueReusableCell(withIdentifier: "AboutMeCell") as! AboutMeCell
            
            cell.aboutMe.text = "\(hospitals!.hospitalTotalBeds!)"
            return cell
        }
        else if indexPath.section == 4 {
            
            let cell : LocationsCell = tableView.dequeueReusableCell(withIdentifier: "LocationsCell") as! LocationsCell
            
            if hospitals != nil {
                
                    let latitude = hospitals!.hospitalLat!
                    let longitude = hospitals!.hospitalLng!
                    let position = CLLocationCoordinate2D(latitude: latitude.doubleValue()!, longitude: longitude.doubleValue()!)
                    let marker = GMSMarker(position: position)
                    marker.title = hospitals!.hospitalName!
                    marker.map = cell.mapView
                
                let camera = GMSCameraPosition.camera(withLatitude: latitude.doubleValue()!, longitude: longitude.doubleValue()!, zoom: 16)
                cell.mapView.camera = camera
            }
            
            
            
            return cell
            
        }
        return UITableViewCell()
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == locationSection{
            return 200
        }
        else if indexPath.section == servicesSection {
            
            if indexPath.row == 0 {
                return UITableViewAutomaticDimension
            }
            else {
                if isThirdSectionExpanded == true {
                    return UITableViewAutomaticDimension
                }
                else {
                    return 0
                }
            }
        }
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    
}





extension HospitalInfoVC : UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "About :"
            return cell
        }
        else if section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Accreditation &  Affiliations:"
            return cell
        }
        else if section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Timings"
            return cell
        }
        else if section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Space"
            return cell
        }
        else if section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Hospital Location"
            return cell
        }
        else if section == 5 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TimingHeaderCell") as! TimingHeaderCell
            cell.headerImage.image = #imageLiteral(resourceName: "clock")
            cell.headerTitle.text = "Services"
            return cell
        }
        else { return UITableViewCell() }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
        
        if section == timingSection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeFooterCell") as! PracticeFooterCell
            
            if isFirstSectionExpanded {
                
                cell.viewDetailsButton.setTitle("View Less Timings", for: .normal)
            }
            else {
                cell.viewDetailsButton.setTitle("View All Timings", for: .normal)
            }
            
            cell.viewDetailsButton.addTarget(self, action: #selector(PracticeVC.showHideTimingList(_:)), for: .touchDown)
            return cell
        }
        else if section == servicesSection {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PracticeFooterCell") as! PracticeFooterCell
            
            if isThirdSectionExpanded {
                
                cell.viewDetailsButton.setTitle("View Less Services", for: .normal)
            }
            else {
                cell.viewDetailsButton.setTitle("View All Services", for: .normal)
            }
            
            cell.viewDetailsButton.addTarget(self, action: #selector(PracticeVC.showHideServicesList(_:)), for: .touchDown)
            return cell
        }
        else {
            return nil
        }
        
    }
    @objc func showHideTimingList(_ sender : UIButton) {
        
        
        tableView.beginUpdates()
        isFirstSectionExpanded = !isFirstSectionExpanded
        tableView.endUpdates()
        tableView.reloadData()
        
    }
    @objc func showHideServicesList(_ sender : UIButton) {
        
        tableView.beginUpdates()
        isThirdSectionExpanded = !isThirdSectionExpanded
        tableView.endUpdates()
        tableView.reloadData()
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    
}

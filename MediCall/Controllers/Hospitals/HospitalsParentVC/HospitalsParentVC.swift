
import UIKit
import Parchment

class HospitalsParentVC: UIViewController {

    
    var pagingController: FixedPagingViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        
        
        let storyBoard = UIStoryboard.init(name: "HospitalsStoryboard", bundle: nil)
        
        let allHospitalsVC = storyBoard.instantiateViewController(withIdentifier: "AllHospitalsListingVC")
        let nearByHospitals = storyBoard.instantiateViewController(withIdentifier: "AllHospitalsListingVC")
        let discounts = storyBoard.instantiateViewController(withIdentifier: "AllHospitalsListingVC")

//
//
        allHospitalsVC.title = "All Hopitals"
        nearByHospitals.title = "Nearby Hospitals"
        discounts.title = "Discounts"
        
        pagingController = HospitalsParentVC.configueTopTabBar(with: [allHospitalsVC,nearByHospitals,discounts], in: self)
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}





extension HospitalsParentVC {
    
    class func configueTopTabBar(with viewControllers : [UIViewController],in viewController : UIViewController) -> FixedPagingViewController
    {
        let view = viewController.view!
        let pagingViewController = FixedPagingViewController(viewControllers: viewControllers)
        
        // Make sure you add the PagingViewController as a child view
        // controller and contrain it to the edges of the view.
        
        viewController.addChildViewController(pagingViewController)
        
        view.addSubview(pagingViewController.view)
        
        pagingViewController.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.selectedFont = UIFont.systemFont(ofSize: 15, weight: .bold)
        pagingViewController.collectionView.bounces = false
        pagingViewController.options.textColor = .white
        pagingViewController.options.selectedTextColor = .white
        pagingViewController.indicatorColor = .white
        pagingViewController.options.borderColor = .white
        pagingViewController.options.selectedTextColor = .white
        view.constrainToEdges(pagingViewController.view, with: 0)
        pagingViewController.indicatorOptions = .visible(height: 3, zIndex: 0, spacing: UIEdgeInsets.zero, insets: UIEdgeInsets.zero)
        pagingViewController.options.menuInsets = UIEdgeInsets.init(top: -5, left: 0, bottom: 0, right: 0)
        pagingViewController.options.menuItemSize = PagingMenuItemSize.sizeToFit(minWidth: pagingViewController.options.menuItemSize.width - 20, height: pagingViewController.options.menuItemSize.height + 5)
        pagingViewController.options.menuItemSpacing = 0
        pagingViewController.options.includeSafeAreaInsets = false
        pagingViewController.options.borderColor = .clear
        
        
        pagingViewController.options.backgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        pagingViewController.options.selectedBackgroundColor = UIColor(red:0.07, green:0.16, blue:0.30, alpha:1.0)
        
        pagingViewController.didMove(toParentViewController: viewController)
        
        return pagingViewController
        
    }
    
}



import UIKit
import XLPagerTabStrip
import CoreLocation
import Alamofire
import AlamofireImage
import Kingfisher
import QuartzCore
import AZTableView

class PanelDoctorsVC: AZTableViewController,CLLocationManagerDelegate {
    
    var dataArray = [AllDoctor]()
    var dataSourceArray = [Doctors]()
    var docSpeciality  = Specilatie()
    var hospital = [Hospitals]()
    var spe = [ExperienceStatus]()
    var land = Doctors()
    var empty = [String]()
    var s = ExperienceStatus()
    var d = Doctors()
    let locManager = CLLocationManager()
    var corrdinateFromApi:CLLocation?
    var myCooridinateslat:Double?
    var myCooridinateslong:Double?
    var offSet = 0
    public var id:Int?
    public var Doctorid:Int?
    var pageNum = 1
    
    
    //@IBOutlet weak var allDoctorsTableView: UITableView!
    //    @IBOutlet weak var allDoctorsTableView: UITableView!
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        
        
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.startMonitoringSignificantLocationChanges()
        guard let latitude = locManager.location?.coordinate.latitude else {return}
        guard let longitude = locManager.location?.coordinate.longitude else {return}
        myCooridinateslat = latitude as! Double
        myCooridinateslong = longitude as! Double
        // print(latitude)
        // print(longitude)
        
//        print(UserDefaults.standard.value(forKey: "userId")!)
        docSpeciality.speciality_id = UserDefaults.standard.value(forKey: "userId") as? Int
//        print(docSpeciality.speciality_id!)
        fetchData()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"listingbackground")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        self.tableView!.backgroundView = imageView
        tableView!.tableFooterView = UIView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFill
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "doctorDetails" {
            
            let destinationVC = segue.destination as! DoctorDetailParentVC
            if sender != nil {
                
                destinationVC.doctorDetails = sender as! Doctors
            }
            
            
        }
        else {
            if let ctrl = segue.destination as?  DoctorDetailViewController
                
            {
                ctrl.doctor_Id  = Doctorid
            }
        }
        
        
    }
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
    
}





extension PanelDoctorsVC: UITableViewDelegate {
    
    
    
    override func AZtableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if dataSourceArray == nil || dataSourceArray.count == 0 {
            return 0
        }
        return dataSourceArray.count
    }
    
    override func AZtableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if dataSourceArray == nil || dataSourceArray.count == 0 {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "alldoctors") as! AllDoctorsTableViewCell
        
        if dataSourceArray[indexPath.row].doctor_full_name!.contains(st:"Dr. ") {
            cell.drNameLbl.text =  dataSourceArray[indexPath.row].doctor_full_name!
        }
        else {
            cell.drNameLbl.text = "Dr. " + dataSourceArray[indexPath.row].doctor_full_name!
        }
        
        let spe = dataSourceArray [indexPath.row]
        
//        for spet in spe.hospitals! {
//
//            //print(spet.hospitals)
//
//            for lands in (spet.hospitals?.landline)!{
//
//                //print(lands.hospital_landline_number!)
//            }
//
//
//        }
        
        let designation = dataSourceArray[indexPath.row]
        for deisg in designation.speciality! {
            cell.drDesignationLbl.text = deisg.speciality?.speciality_designation
        }
        
        let minFee = Int(dataSourceArray[indexPath.row].doctor_min_fee ?? "0")
        let maxFee = Int(dataSourceArray[indexPath.row].docor_max_fee ?? "0")
        let fee = minFee! - maxFee!
        
        let f = String(fee)
        
        cell.drFeeLbl.text = "Fee: " + f
        
        let lat = dataSourceArray[indexPath.row]
        for lati in lat.hospitals! {
            // print (lati.hospitals?.hospital_lat)
            let a = lati.hospitals?.hospital_lng
            let b = Double(a!)
            //print(b!)
            let c = lati.hospitals?.hospital_lat
            let d = Double(c!)
            // print(d!)
            var apiLocation = CLLocation(latitude: d!, longitude: b!)
            var myLocation = CLLocation(latitude: myCooridinateslat!, longitude: myCooridinateslong!)
            var distance = myLocation.distance(from: apiLocation) / 1000
            var distanceinInt = distance
            
            //print(distanceinInt)
            let y = Double(distanceinInt).rounded(toPlaces:1)
            cell.distanceLbl.text = String(y)
            
            let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + dataSourceArray[indexPath.row].doctor_img!
            //            let url = URL(fileURLWithPath: imageUrl)
            //            cell.drProfileImageView.af_setImage(withURL: url)
            // let imageUrl = "http://themedicall.com/public/uploaded/img/specialities/" + dataSourceArray[indexPath.row].speciality_icon!
            
            let url = URL(string: imageUrl)
            cell.drProfileImageView.af_setImage(withURL: url!)
            cell.drProfileImageView.roundImage()
        }
        
        
        for lati in lat.hospitals! {
            
            let img = lati.hospitals?.hospital_img
            //print(img)
            let imageUrl1 = "http://themedicall.com/public/uploaded/img/hospitals/" + (lati.hospitals?.hospital_img)!
            let url = URL(string: imageUrl1)
            //print(url)
            cell.hospitalImage.af_setImage(withURL: url!)
            cell.hospitalDescriptionLbl.text = lati.hospitals?.hospital_name
            
            
        }
        let spe2 = dataSourceArray[indexPath.row]
        var a = String()
        
        for Speciality in spe2.qualifications! {
            
            var b = (Speciality.qualifications?.qualification_title)! + ","
            
            a.append(b)
            
            print(a)
        }
        if a.contains(st: ",") {
            a.removeLast() }
        cell.ratingLbl.text = a
        if dataSourceArray[indexPath.row].doctor_verified_status != 1 {
            cell.verifyDocImage.image = nil
        }
        
        cell.experienceLbl.text = dataSourceArray[indexPath.row].doctor_experience!
        
        cell.expLblView.setCornerRadius(r:5)
        
        
        if dataSourceArray[indexPath.row].doctor_offer_any_discount == "Yes" {
            cell.discountedImageView.image = #imageLiteral(resourceName: "discounted")
        }
        cell.backgroundColor = UIColor.clear
        return  cell
    }
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(dataSourceArray[indexPath.row].doctor_id)
        Doctorid = dataSourceArray[indexPath.row].doctor_id
        
        UserDefaults.standard.set(Doctorid, forKey: "doctor_id")
        
        let doctorDetails = dataSourceArray[indexPath.row]
        
//        self.performSegue(withIdentifier: "doctorDetails", sender:doctorDetails)
        //        self.performSegue(withIdentifier: "doctorSb", sender:Doctorid)
    }
    
    
    
    
    
    func getListOfDoctors() {
        
        
        ApiManager.shared.tryFetchPanelDoctors(id: 48, offset: self.pageNum, success: { [weak self] (doctors) in
            
            guard let weakSelf = self else { return }
            LoaderManager.hide(weakSelf.view)
            if weakSelf.dataSourceArray.count == 0 || weakSelf.dataSourceArray == nil {
                weakSelf.dataSourceArray = doctors!.doctors!
            }
            else {
                weakSelf.dataSourceArray.append(contentsOf: doctors!.doctors!)
            }
            
            if weakSelf.dataSourceArray.count < doctors!.total {
                weakSelf.pageNum = weakSelf.pageNum + 1
                weakSelf.didfetchData(resultCount: weakSelf.dataSourceArray.count, haveMoreData: true)
            }
            else {
                weakSelf.didfetchData(resultCount: weakSelf.dataSourceArray.count, haveMoreData: false)
            }
            
            
            
        }) { [weak self] (error) in
            guard let weakSelf = self else { return }
            LoaderManager.hide(weakSelf.view)
            ToastManager.showErrorMessage(message: error.localizedDescription)
        }
        
    }
    
    
    
    
}




extension PanelDoctorsVC{
    
    
    override func fetchData() {
        super.fetchData()
        LoaderManager.show(self.view)
        self.dataSourceArray.removeAll()
        self.tableView!.reloadData()
        pageNum = 1
        getListOfDoctors()
    }
    
    
    
    override func fetchNextData() {
        super.fetchNextData()
        getListOfDoctors()
    }
    
    
    
    
}


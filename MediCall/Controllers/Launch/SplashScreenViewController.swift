
import UIKit
import PureLayout
import SwiftGifOrigin
import Kingfisher
import RealmSwift
import Realm


protocol SplashScreenDelegate: class {
    func splashScreenDone()
}

class SplashScreenViewController: UIViewController {
   
    let iconImgView = UIImageView()
    var constraintsAdded = false
    var citiesArray = [Cities]()
    
    weak var delegate: SplashScreenDelegate?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    init(with delegate: SplashScreenDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.delegate = delegate
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white
        
        iconImgView.loadGif(name: "splashscreen")
        iconImgView.clipsToBounds = true
        iconImgView.contentMode = .scaleAspectFit
        
        view.addSubview(iconImgView)
        view.setNeedsUpdateConstraints()
        validator()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        if !constraintsAdded {
            constraintsAdded = true
            iconImgView.autoAlignAxis(toSuperviewAxis: .horizontal)
            iconImgView.autoAlignAxis(toSuperviewAxis: .vertical)
            iconImgView.autoSetDimensions(to: CGSize(width:750, height: 750))
        }
    }
    
    func navToMainPage() {
        self.delegate?.splashScreenDone()
    }
    
    func fetchCities() {
        let apiCall = ApiManager.shared.getCities()
            apiCall.then {
                cities -> Void in
                
                let realm = try Realm()
                try realm.write {
                    for city in cities {
                        realm.add(city,update: true)
                    }
                }
                self.delegate?.splashScreenDone()
                }.catch {
                    error -> Void in
                    print("some error in json parsing")
            }
        }
    
    func validator() {
        let realm = try! Realm()
        if realm.isEmpty {
            fetchCities()
        }
        else {
            navToMainPage()
        }
    }
    
    func putinDatabase() {
        
//        add()
//
//        var a = LocalCities()
//
//        for city in citiesArray {
//            //local.append(city)
//        }
//        let realm = try! Realm()
//        var allCITY = realm.objects(a)
//        try! realm.write {
//            realm.add(a)
//        }
        
        //let newArray = NSKeyedArchiver.archivedData(withRootObject: citiesArray)
        //let newArray = citiesArray as NSArray
        
        
       // print(citiesArray.count)
//        let local = LocalCities()
//        var local2 = [LocalCities]()
       // let path = Bundle.main.path(forResource: "db", ofType: "sqlite3")!
        //AppDelegate.MyVariables.yourVariable = citiesArray
        
        //_ = try Connection(path, readonly: true)
//        var local = citiesArray
//        for lo in local {
//            print(lo.city_id!)
//            print(lo.city_title!)
//        }
//
        
            //local.Cityid = cityeo.city_id!
            //local.title = cityeo.city_title!
//            //print(local.Cityid)
//           // print(local.title)
//        for cityeo in localArray {
//           print(cityeo.city_title!)
//        print(cityeo.city_id!)
//        }
//            //local2.append(local)
        
            //}
        
       // let a = [LocalCities]()
       // DataManager.shared.insertOrUpdateMessage(citiesArray)
       // print(local2.count)
       
       
    }
}



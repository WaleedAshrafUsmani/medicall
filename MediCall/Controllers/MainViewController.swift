

import UIKit
import Realm
import RealmSwift
import QuartzCore
import SideMenuSwift
import CoreLocation

class MainViewController: MCOverlayViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var currentCityBarButtonItem: UIBarButtonItem!
    var citiesArray = [Cities]()
    var a = ""
    var city = [Cities]()
    let locationManager = CLLocationManager()
    
    
    @IBOutlet weak var mainBtn: UIButton!
    @IBOutlet weak var blurrView: UIVisualEffectView!
    
    @IBOutlet weak var viewLeadingCst: NSLayoutConstraint!
    @IBOutlet weak var sideView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_menu_button"), style: .plain, target: self, action: #selector(showSideMenu))
        self.navigationItem.title = "Home"
        
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        
        
        getCurrentCity()
        

        //        mainBtn.setTitle("some", for: UIControlState.normal)
//        mainBtn.backgroundColor = Colors.white
//        super.viewDidLoad()
//        self.view.addTapToDismiss()
//
//        let userImage = UserDefaultManager.shared.readUser().profileImage
//
//        let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + (userImage ?? "")
//        print(imageUrl)
//        let url = URL(string: imageUrl)
//        profileImageView.af_setImage(withURL: url!)
        
        
       
       // fetchCities()
        //city = getNotes()
//        for cit in city {
//            print(cit.city_title)
//        }
   //mainBtn.setTitle(a, for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    func getCurrentCity() {
        
        LocationServicesManager.shared.getAdress { address, error in
            
            if let address = address, let city = address["City"] as? String {
                
                print(city)
                self.currentCityBarButtonItem.title = city
                
            }
        }
    }
    
    @IBAction func showSideMenu(_ sender: UIButton) {
        sideMenuController?.revealMenu()
    }
    @IBAction func changeCurrentCity(_ sender: Any) {
        
        self.performSegue(withIdentifier: "ShowCitites", sender: self)
        
    }
    
    @IBAction func showDoctorsListing(_ sender: UIButton) {
        performSegue(withIdentifier: "doctorListing", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCitites" {
            
            let controller = segue.destination as! PopUpViewController
            controller.citySelectionDelegate = self
            
        }
    }
    
}
    
    
//    func fetchCities() {
//
//        let apiCall = ApiManager.shared.getCities()
//        LoaderManager.show(self.view)
//        apiCall.then {
//
//            cities -> Void in
//            LoaderManager.hide(self.view)
//            self.citiesArray = cities
//            //self.putinDatabase()
//            //self.navToMainPage()
//            //self.allDoctorsTableView.reloadData()
//            }.catch {
//                error -> Void in
//
//        }
//    }
  




extension MainViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            break
            
        case .denied:
            break
            
        case .authorizedWhenInUse:
            break
            
        case .authorizedAlways:
//            getCurrentCity()
            break
        default:
            break
        }
        
    }
    
}







extension MainViewController : CitySelectionDelegate {
    
    
    
    func didSelectCity(city: String?, cityId: Int?) {
        
        
        self.currentCityBarButtonItem.title = city
        
    }
    
    
    
    
}

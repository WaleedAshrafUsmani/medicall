
import UIKit
import XLPagerTabStrip
import CoreLocation
import Alamofire
import AlamofireImage
import Kingfisher
import QuartzCore
import AZTableView

class NearByViewController: AZTableViewController,CLLocationManagerDelegate {
    
    var dataArray = [AllDoctor]()
    var dataSourceArray = [Doctors]()
    var docSpeciality  = Specilatie()
    var hospital = [Hospitals]()
    var spe = [ExperienceStatus]()
    var land = Doctors()
    var empty = [String]()
    var s = ExperienceStatus()
    var d = Doctors()
    let locManager = CLLocationManager()
    var corrdinateFromApi:CLLocation?
    var myCooridinateslat:Double?
    var myCooridinateslong:Double?
    var offSet = 0
    public var id:Int?
    public var Doctorid:Int?
    var pageNum = 1
    
    
    //@IBOutlet weak var allDoctorsTableView: UITableView!
    //    @IBOutlet weak var allDoctorsTableView: UITableView!
    
    // MARK: - IBOutlets
    
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        
        
        
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.startMonitoringSignificantLocationChanges()
        guard let latitude = locManager.location?.coordinate.latitude else {return}
        guard let longitude = locManager.location?.coordinate.longitude else {return}
        myCooridinateslat = latitude as! Double
        myCooridinateslong = longitude as! Double
        // print(latitude)
        // print(longitude)
        
        print(UserDefaults.standard.value(forKey: "userId")!)
        docSpeciality.speciality_id = UserDefaults.standard.value(forKey: "userId") as? Int
        print(docSpeciality.speciality_id!)
        fetchData()
        
        
        //        let imageBackground = UIColor(patternImage: UIImage(named: "listingbackground")!)
        //let tempImageView = UIImageView(image: UIImage(named: "listingbackground"))
        //tempImageView.frame = self.allDoctorsTableView.frame
        //self.allDoctorsTableView.backgroundView = tempImageView;
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let imageView : UIImageView = {
            let iv = UIImageView()
            iv.image = UIImage(named:"listingbackground")
            iv.contentMode = .scaleAspectFill
            return iv
        }()
        self.tableView!.backgroundView = imageView
        tableView!.tableFooterView = UIView(frame: CGRect.zero)
        imageView.contentMode = .scaleAspectFill
        
        //        LoaderManager.show(self.view)
        //        let ApiCall =  ApiManager.shared.fetchAllDoctors(id: docSpeciality.speciality_id!, offset: offSet)
        //        ApiCall.then {
        //            doctors -> Void in
        //            LoaderManager.hide(self.view)
        //            self.dataSourceArray = doctors
        //            self.allDoctorsTableView.reloadData()
        //            }.catch {
        //                error -> Void in
        //        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "doctorDetails" {
            
            let destinationVC = segue.destination as! DoctorDetailParentVC
            if sender != nil {
                
                destinationVC.doctorDetails = sender as! Doctors
            }
            
            
        }
        else {
            if let ctrl = segue.destination as?  DoctorDetailViewController
                
            {
                ctrl.doctor_Id  = Doctorid
            }
        }
        
        
    }
    
    
    // MARK: - Functions
    
    
    
    
    // MARK: - IBActions
    
    
}





extension NearByViewController: UITableViewDelegate {
    
    
    
    override func AZtableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSourceArray.count
    }
    
    override func AZtableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "alldoctors") as! AllDoctorsTableViewCell
        
        
        //cell.drNameLbl.text = dataSourceArray[indexPath.row].hospitals?[indexPath.row].hospitals?.hospital_name
        //cell.drNameLbl.text = dataSourceArray[indexPath.row].experience_status?.experience_status_title
        if dataSourceArray[indexPath.row].doctor_full_name!.contains(st:"Dr. ") {
            cell.drNameLbl.text =  dataSourceArray[indexPath.row].doctor_full_name!
        }
        else {
            cell.drNameLbl.text = "Dr. " + dataSourceArray[indexPath.row].doctor_full_name!
        }
        
        let spe = dataSourceArray [indexPath.row]
        
        for spet in spe.hospitals! {
            
            //print(spet.hospitals)
            
            for lands in (spet.hospitals?.landline)!{
                
                //print(lands.hospital_landline_number!)
            }
            
            
        }
        //print(dataSourceArray[indexPath.row].experience_status?.experience_status_title)
        let designation = dataSourceArray[indexPath.row]
        for deisg in designation.speciality! {
            cell.drDesignationLbl.text = deisg.speciality?.speciality_designation
        }
        //        print(dataSourceArray[indexPath.row].docor_max_fee)
        //        print(dataSourceArray[indexPath.row].doctor_min_fee)
        let minFee = Int(dataSourceArray[indexPath.row].doctor_min_fee ?? "0")
        let maxFee = Int(dataSourceArray[indexPath.row].docor_max_fee ?? "0")
        let fee = minFee! - maxFee!
        //        if minFee == maxFee {
        //            cell.drFeeLbl.text = String(maxFee!)
        //        }
        //        else {
        //            cell.drFeeLbl.text =  String(maxFee!) + "-"  + String(minFee!)
        //        }
        //print(fee)
        let f = String(fee)
        //rint(f)
        cell.drFeeLbl.text = "Fee: " + f
        
        //cell.drFeeLbl.text = "some text"
        //cell.drProfileImageView.image = #imageLiteral(resourceName: "doctor")
        //cell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
        //cell.drNameLbl.text = "aaafff"
        let lat = dataSourceArray[indexPath.row]
        for lati in lat.hospitals! {
            // print (lati.hospitals?.hospital_lat)
            let a = lati.hospitals?.hospital_lng
            let b = Double(a!)
            //print(b!)
            let c = lati.hospitals?.hospital_lat
            let d = Double(c!)
            // print(d!)
            var apiLocation = CLLocation(latitude: d!, longitude: b!)
            var myLocation = CLLocation(latitude: myCooridinateslat!, longitude: myCooridinateslong!)
            var distance = myLocation.distance(from: apiLocation) / 1000
            var distanceinInt = distance
            
            //print(distanceinInt)
            let y = Double(distanceinInt).rounded(toPlaces:1)
            cell.distanceLbl.text = String(y)
            
            let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + dataSourceArray[indexPath.row].doctor_img!
            //            let url = URL(fileURLWithPath: imageUrl)
            //            cell.drProfileImageView.af_setImage(withURL: url)
            // let imageUrl = "http://themedicall.com/public/uploaded/img/specialities/" + dataSourceArray[indexPath.row].speciality_icon!
            
            let url = URL(string: imageUrl)
            cell.drProfileImageView.af_setImage(withURL: url!)
            cell.drProfileImageView.roundImage()
        }
        // print(lat)
        
        //           let latitude =  lati.hospitals?.hospital_lat
        //            let longtude = lati.hospitals?.hospital_lng
        //            //corrdinateFromApi = CLLocation(latitude: latitude, longitude: longtude)
        //           print(latitude)
        //            print(longtude)
        //}
        
        for lati in lat.hospitals! {
            
            let img = lati.hospitals?.hospital_img
            //print(img)
            let imageUrl1 = "http://themedicall.com/public/uploaded/img/hospitals/" + (lati.hospitals?.hospital_img)!
            let url = URL(string: imageUrl1)
            //print(url)
            cell.hospitalImage.af_setImage(withURL: url!)
            cell.hospitalDescriptionLbl.text = lati.hospitals?.hospital_name
            
            
        }
        let spe2 = dataSourceArray[indexPath.row]
        var a = String()
        
        for Speciality in spe2.qualifications! {
            
            var b = (Speciality.qualifications?.qualification_title)! + ","
            
            a.append(b)
            
            print(a)
        }
        if a.contains(st: ",") {
            a.removeLast() }
        cell.ratingLbl.text = a
        if dataSourceArray[indexPath.row].doctor_verified_status != 1 {
            cell.verifyDocImage.image = nil
        }
        //        let myString =
        //        let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.red ]
        //        let myAttrString = NSAttributedString(string: myString!, attributes: myAttribute)
        cell.experienceLbl.text = dataSourceArray[indexPath.row].doctor_experience!
        //        profileDescription.setCornerRadius(r: 8)
        //        profileDescription.setBorderWidth(width: 1)
        //        profileDescription.setBorderColor(color: UIColor(0xE2E2E2))
        cell.expLblView.setCornerRadius(r:5)
        
        //cell.experienceLbl.cornerRadius = 50
        if dataSourceArray[indexPath.row].doctor_offer_any_discount == "Yes" {
            cell.discountedImageView.image = #imageLiteral(resourceName: "discounted")
        }
        cell.backgroundColor = UIColor.clear
        return  cell
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        cell.backgroundColor = UIColor.clear
    //        guard dataSourceArray.count<10 else {
    //            if indexPath.row == dataSourceArray.count - 1  {
    //                //print(offSet)
    //                moreDataFromApi()
    //            }
    //            else {
    //                return
    //            }
    //            return
    //        }
    //    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //print(dataSourceArray[indexPath.row].doctor_id)
        Doctorid = dataSourceArray[indexPath.row].doctor_id
        
        UserDefaults.standard.set(Doctorid, forKey: "doctor_id")
        
        let doctorDetails = dataSourceArray[indexPath.row]
        
        self.performSegue(withIdentifier: "doctorDetails", sender:doctorDetails)
//        self.performSegue(withIdentifier: "doctorSb", sender:Doctorid)
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        //print(dataSourceArray[indexPath.row].doctor_id)
//        Doctorid = dataSourceArray[indexPath.row].doctor_id
//
//        UserDefaults.standard.set(Doctorid, forKey: "doctor_id")
//
//        self.performSegue(withIdentifier: "doctorSb", sender:Doctorid)
//
//    }
    
    
    
    
    //    func moreDataFromApi() {
    //
    //
    //        offSet += 10
    //        LoaderManager.show(self.view)
    //        let ApiCall =  ApiManager.shared.fetchAllDoctors(id: docSpeciality.speciality_id!, offset: offSet)
    //        ApiCall.then {
    //            doctors -> Void in
    //            LoaderManager.hide(self.view)
    //            self.dataSourceArray += doctors
    //            self.allDoctorsTableView.reloadData()
    //            }.catch {
    //                error -> Void in
    //                LoaderManager.hide(self.view)
    //        }
    //    }
    
    
    func getListOfDoctors() {
        
        
        ApiManager.shared.tryFetchNearByDoctors(id: docSpeciality.speciality_id!, offset: self.pageNum, success: { [weak self] (doctors) in
            
            guard let weakSelf = self else { return }
            LoaderManager.hide(weakSelf.view)
            if weakSelf.dataSourceArray.count == 0 || weakSelf.dataSourceArray == nil {
                weakSelf.dataSourceArray = doctors!.doctors!
            }
            else {
                weakSelf.dataSourceArray.append(contentsOf: doctors!.doctors!)
            }
            
            if weakSelf.dataSourceArray.count < doctors!.total {
                weakSelf.pageNum = weakSelf.pageNum + 1
                weakSelf.didfetchData(resultCount: weakSelf.dataSourceArray.count, haveMoreData: true)
            }
            else {
                weakSelf.didfetchData(resultCount: weakSelf.dataSourceArray.count, haveMoreData: false)
            }
            
            
            
        }) { [weak self] (error) in
            guard let weakSelf = self else { return }
            LoaderManager.hide(weakSelf.view)
            ToastManager.showErrorMessage(message: error.localizedDescription)
        }
        
    }
    
    
    
    
}




extension NearByViewController{
    
    
    override func fetchData() {
        super.fetchData()
        LoaderManager.show(self.view)
        self.dataSourceArray.removeAll()
        self.tableView!.reloadData()
        pageNum = 1
        getListOfDoctors()
    }
    
    
    
    override func fetchNextData() {
        super.fetchNextData()
        getListOfDoctors()
    }
    
    
    
    
}






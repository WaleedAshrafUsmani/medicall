
import UIKit
import RealmSwift
import Realm

protocol CitySelectionDelegate {
    func didSelectCity(city:String?,cityId:Int?)
}

class PopUpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate{
    
    
    
    var cities = [Cities]()
    var citi = ["1","2","3","4","5"]
    var identity = 0
    var selectedCity:String?
    var citySelectionDelegate:CitySelectionDelegate?
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var myTableView: UITableView!
    var filterwaladata = [Cities]()
    var filterdData = "" {
        didSet {
            //filterdData = cities.filter({$})
            
            filterwaladata = cities.filter({ (cities) -> Bool in
                (cities.city_title.lowercased().contains(st: filterdData))
                
            })
            myTableView.reloadData()
        }
    }
   
    var isSearching = false
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filterwaladata.count
        }
        else {
       return cities.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"pop", for: indexPath) as! PopUpTableViewCell
        if isSearching {
            cell.CityNameLbl.text = filterwaladata[indexPath.row].city_title
            return cell
            
        }
        cell.CityNameLbl.text = cities[indexPath.row].city_title
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        identity = indexPath.row
       
        citySelectionDelegate?.didSelectCity(city: cities[indexPath.row].city_title, cityId: cities[indexPath.row].city_id)
           // selectedCity = cities[indexPath.row].city_title
        if isSearching {
            citySelectionDelegate?.didSelectCity(city: filterwaladata[indexPath.row].city_title, cityId: filterwaladata[indexPath.row].city_id)
        }
        
     //self.performSegue(withIdentifier: "City", sender:identity)
     //AppDelegate.MyVariables.yourVariable = cities[identity].city_title
        self.dismiss(animated: true, completion: nil)
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        
        if let ctrl = segue.destination as? UserFormViewController {
            
            //ctrl.a = cities[identity].city_title
            ctrl.cityLbl.text = cities[identity].city_title
            
            
        }
        
    }
    
   
    override func viewDidLoad() {
        
        
//        if let data = UserDefaults.standard.value(forKey: "cities") as? Data {
//            if let dict = NSKeyedUnarchiver.unarchiveObject(with: data) as? Cities{
//                print(dict)
//            }
//        }
        searchBar.delegate = self
        super.viewDidLoad()
        cities = getNotes()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            //myTableView.reloadData()
        }
            else  {
                isSearching = true
            filterdData = searchBar.text?.lowercased() ?? ""
                
            }
            
        }
    
    func getNotes() -> [Cities]{
        
        //let local = LocalCities()
        //let model = Cities()
        //let user = UserDefaultManager.shared.readUser()
        let realm = try! Realm()
        //         DataManager.shared.getNotes()
        let cities = realm.objects(Cities.self)
        return Array(cities)
        //let a = Array(cities)
        //print(a.count)
        //        for city in a {
        //            print(city.title)
        //        }
        
        //self.navToMainPage()
        
    }
    
    }
    
   



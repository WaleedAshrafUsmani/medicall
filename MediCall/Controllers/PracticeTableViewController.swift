
import UIKit

class PracticeTableViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    

    
    @IBOutlet weak var practiceTableView: UITableView!
    
    var buttonTag = 0
    var viewCount = [1]
    var dayCount = [1]
    var addCityBtn = UIButton()
    var index:IndexPath?
    var insideTableView = UITableView()
    var weekLabel = ["▼","▼","▼"]
    var discountView = UIView()
    var filterString : String = "" {
        didSet {
            practiceTableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
       practiceTableView.tableFooterView = UIView(frame: CGRect.zero)
        insideTableView.tableFooterView = UIView(frame: CGRect.zero)
        insideTableView.estimatedRowHeight = 55
        insideTableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 90
        {
            return viewCount.count
            
            
        }
    
        return dayCount.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        if tableView.tag == 90 {
            
        let cell = tableView.dequeueReusableCell(withIdentifier: "practiceCell", for: indexPath) as! PracticeTableViewCell
            insideTableView = cell.insideTableView
            //cell.showDiscountPackageBtn
            if viewCount.count != 1 {
            cell.deletePracticeButton.addTarget(self, action:#selector(deletePractice(_:)), for: .touchUpInside)
                cell.disCountBtn.setTitle("some", for: .normal)
               
                
            }
            else {
                cell.deletePracticeButton.addTarget(self, action:#selector(showError(_:)), for: .touchUpInside)
            }
            discountView = cell.discountView
        cell.disCountBtn.addTarget(self, action:#selector(showDiscount(_:)), for: .touchUpInside)
        cell.deletePracticeButton.tag = indexPath.row
        //index = indexPath
            return cell
            
            
            
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "practiceInside", for: indexPath) as! PracticeInsideTableViewCell
        cell.textLbl.text = "Some City"
        cell.addNewDayBtn.setTitle("Some", for: .normal)
        
            
            cell.addNewDayBtn.addTarget(self, action: #selector(addNewDay(_:)), for: .touchUpInside)
            addCityBtn =  cell.addNewDayBtn
        
        
        
        cell.addNewDayBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 90 {
            return 330 }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index = indexPath
    }
    @IBAction func addNewPractice(_ sender: UIButton) {
        let tag = sender.tag
        viewCount.append(tag)
        //viewCount += 1
        let indexPath = IndexPath(row: viewCount.count - 1, section: 0)
        practiceTableView.beginUpdates()
        practiceTableView.insertRows(at: [indexPath], with: .top)
        print(indexPath)
        practiceTableView.endUpdates()
        
        
    }
    
    @objc func addNewDay(_ sender:UIButton) {
        
            let tag = sender.tag
            addCityBtn.setTitle("❌", for: .normal)
            dayCount.append(tag)
            let indexPath = IndexPath(row: dayCount.count - 1, section: 0)
            insideTableView.beginUpdates()
            insideTableView.insertRows(at: [indexPath], with: .bottom)
            insideTableView.endUpdates()
            insideTableView.reloadData()
        }
    
       
    
    
    
    @objc func deletePractice(_ button: UIButton){
        let buttonTag = button.tag
        viewCount.remove(at: buttonTag)
        let indexPath = IndexPath(row:buttonTag , section: 0)
        practiceTableView.beginUpdates()
        practiceTableView.deleteRows(at: [indexPath], with: .top)
        practiceTableView.endUpdates()
        //viewCount -= 1
        
       
    }
    @objc func showError(_ button:UIButton) {
        ToastManager.showInfoMessage(message: "There is nothing to remove")
    }
    
    @objc func showDiscount(_ button:UIButton) {
        print("doooooooo")
        practiceTableView.beginUpdates()
        if discountView.alpha == 1 {
            discountView.alpha = 0
        }
        else
        {
            discountView.alpha = 1
        }
        practiceTableView.endUpdates()
        //practiceTableView.reloadData()
    }
}



import UIKit
import SnapKit
import QuartzCore

class PracticeViewController: UIViewController {
    
    

    
    
    lazy var box = UIView()
     var dottedBorderView = UIView()
    var buttonTag = 0
    
    
    @IBOutlet weak var mainScroll: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MainView(400, 10)
        
    }
   
    func MainView(_ mainHieght:Int,_ topCst:Int) {
        
        
        let superView = self.view!
        
        box.backgroundColor = Colors.green
        let mainView = UIStackView()
        self.view.addSubview(mainView)
        
        
        mainScroll.addSubview(box)
        //self.view.addSubview(box)
        box.snp.makeConstraints { (make) in
            make.height.equalTo(mainHieght)
            make.top.equalTo(superView.snp.topMargin).offset(topCst)
            make.leading.equalTo(superView.snp.leading).offset(0)
            make.trailing.equalTo(superView.snp.trailing).offset(0)
            
        }
        
        
        
        let addWorkLbl = UILabel()
        addWorkLbl.text = "Add Work Place Information"
        box.addSubview(addWorkLbl)
        addWorkLbl.snp.makeConstraints { (make) in
            make.height.equalTo(30)
            make.top.equalTo(box.snp.top).offset(10)
            //            make.leading.equalTo(box.snp.leading).offset(40)
            //            make.trailing.equalTo(box.snp.trailing).offset(40)
            make.centerX.equalToSuperview()
        }
        dottedBorderView.autoresizesSubviews = true
        dottedBorderView.clearsContextBeforeDrawing = true
        dottedBorderView.isOpaque = true
        
        //        let border = CAShapeLayer()
        //        border.strokeColor = UIColor.black.cgColor
        //        border.fillColor = nil
        //        border.lineDashPattern = [4, 4]
        //        border.path = UIBezierPath(rect: box.bounds).cgPath
        //        border.frame = box.bounds
        //        box.layer.addSublayer(border)
        
        
        //        var yourViewBorder = CAShapeLayer()
        //        yourViewBorder.strokeColor = UIColor.black.cgColor
        //        yourViewBorder.lineDashPattern = [3, 3]
        //        yourViewBorder.frame = dottedBorderView.bounds
        //        yourViewBorder.fillColor = nil
        //        yourViewBorder.path = UIBezierPath(rect: dottedBorderView.bounds).cgPath
        //        dottedBorderView.layer.addSublayer(yourViewBorder)
        
        // dottedBorderView.setBorderColor(color: .black)
        // dottedBorderView.setBorderWidth(width: 2)
        dottedBorderView.backgroundColor = Colors.lightGrey
        box.addSubview(dottedBorderView)
        dottedBorderView.snp.makeConstraints { (make) in
            make.leading.equalTo(box.snp.leading).offset(20)
            make.trailing.equalTo(box.snp.trailing).offset(-20)
            make.top.equalTo(addWorkLbl.snp.bottom).offset(30)
            make.bottom.equalTo(box.snp.bottom).offset(-100)
            
        }
        
        let workPlace = UITextField()
        workPlace.backgroundColor = Colors.white
        dottedBorderView.addSubview(workPlace)
        workPlace.snp.makeConstraints { (make) in
            make.leading.equalTo(dottedBorderView.snp.leading).offset(20)
            make.trailing.equalTo(dottedBorderView.snp.trailing).offset(-20)
            make.top.equalTo(dottedBorderView.snp.top).offset(10)
            make.height.equalTo(45)
            
        }
        let workButton = UIButton()
        workButton.setTitle("☓", for: .normal)
        workButton.setCornerRadius(r:20)
        workButton.backgroundColor = Colors.green
        dottedBorderView.addSubview(workButton)
        workButton.snp.makeConstraints { (make) in
            make.top.equalTo(dottedBorderView.snp.top).offset(5)
            make.trailing.equalTo(dottedBorderView.snp.trailing)
            make.height.width.equalTo(40)
        }
        
        let dayView = UIView()
        let startTime = UITextField()
        dayView.backgroundColor = Colors.white
        box.addSubview(dayView)
        dayView.snp.makeConstraints { (make) in
            make.leading.equalTo(dottedBorderView.snp.leading).offset(20)
            //make.trailing.equalTo(dottedBorderView.snp.trailing).offset(-200)
            make.top.equalTo(workPlace.snp.bottom).offset(10)
            make.height.equalTo(40)
        }
        let dayLabel = UILabel()
        dayLabel.text = "Day"
        dayView.addSubview(dayLabel)
        dayLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(dayView).offset(10)
            make.top.equalTo(dayView).offset(10)
            make.centerX.equalToSuperview()
        }
        
        startTime.backgroundColor = Colors.white
        startTime.placeholder = "Start Time"
        box.addSubview(startTime)
        startTime.snp.makeConstraints { (make) in
            make.leading.equalTo(dayView.snp.trailing).offset(20)
            // make.trailing.equalTo(dottedBorderView.snp.trailing).offset(-20)
            make.top.equalTo(workPlace.snp.bottom).offset(10)
            make.height.equalTo(40)
            make.width.equalTo(100)
            
        }
        
        let endTime = UITextField()
        endTime.placeholder = "End Time"
        endTime.backgroundColor = Colors.white
        box.addSubview(endTime)
        endTime.snp.makeConstraints { (make) in
            make.leading.equalTo(startTime.snp.trailing).offset(20)
            make.trailing.equalTo(dottedBorderView.snp.trailing).offset(-20)
            make.top.equalTo(workPlace.snp.bottom).offset(10)
            make.height.equalTo(40)
            make.width.equalTo(startTime.snp.width)
            
        }
        let minFee = UIView()
        minFee.backgroundColor = Colors.white
        dottedBorderView.addSubview(minFee)
        minFee.snp.makeConstraints { (make) in
            make.leading.equalTo(dottedBorderView).offset(20)
            make.top.equalTo(endTime.snp.bottom).offset(10)
            // make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(40)
        }
        
        let maxFee = UIView()
        maxFee.backgroundColor = Colors.white
        dottedBorderView.addSubview(maxFee)
        maxFee.snp.makeConstraints { (make) in
            make.leading.equalTo(minFee.snp.trailing).offset(20)
            make.top.equalTo(endTime.snp.bottom).offset(10)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(40)
            make.width.equalTo(150)
        }
        
        let discountPackage = UIView()
        discountPackage.backgroundColor = Colors.white
        dottedBorderView.addSubview(discountPackage)
        discountPackage.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.height.equalTo(40)
            make.top.equalTo(maxFee.snp.bottom).offset(10)
        }
        
        let bottomLabelStack = UIStackView()
        let firstLbl = UILabel()
        firstLbl.text = "Check Up"
        let secondLbl = UILabel()
        secondLbl.text = "Procedure"
        let thirdLbl = UILabel()
        thirdLbl.text = "Other"
        
        bottomLabelStack.axis = UILayoutConstraintAxis.horizontal
        bottomLabelStack.distribution = UIStackViewDistribution.equalSpacing
        bottomLabelStack.spacing = 20
        bottomLabelStack.addArrangedSubview(firstLbl)
        bottomLabelStack.addArrangedSubview(secondLbl)
        bottomLabelStack.addArrangedSubview(thirdLbl)
        
        dottedBorderView.addSubview(bottomLabelStack)
        bottomLabelStack.snp.makeConstraints { (make) in
            make.top.equalTo(discountPackage.snp.bottom).offset(10)
            make.leading.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-30)
        }
        
        let percentStackView = UIStackView()
        let percentView = UIView()
        let secondPercentView = UIView()
        let thirdPercentView = UIView()
        
        let percentTextField = UITextField()
        let percentLbl = UILabel()
        percentTextField.placeholder = "10"
        
        percentView.addSubview(percentTextField)
        secondPercentView.addSubview(percentTextField)
        //thirdPercentView.addSubview(percentTextField)
        percentLbl.text = "%"
        
        percentView.addSubview(percentLbl)
        secondPercentView.addSubview(percentLbl)
        //thirdPercentView.addSubview(percentLbl)
        
        percentTextField.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalTo(percentLbl.snp.leading).offset(10)
            make.top.equalToSuperview().offset(20)
        }
        
        
        percentLbl.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
        }
        percentStackView.addArrangedSubview(percentView)
        //percentStackView.addArrangedSubview(secondPercentView)
        //percentStackView.addArrangedSubview(thirdPercentView)
        //percentStackView.addArrangedSubview(percentView)
        
        percentStackView.axis = UILayoutConstraintAxis.horizontal
        percentStackView.distribution = UIStackViewDistribution.equalSpacing
        percentStackView.spacing = 10
        dottedBorderView.addSubview(percentStackView)
        percentStackView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalTo(bottomLabelStack.snp.bottom).offset(20)
            make.height.equalTo(70)
        }
        let addnewButton = UIButton()
        addnewButton.setTitle("Add Another Practice", for: .normal)
        box.addSubview(addnewButton)
        addnewButton.snp.makeConstraints { (make) in
            
            make.height.equalTo(40)
            make.width.equalTo(180)
            //make.top.equalTo(dottedBorderView.snp.bottom)
            make.bottom.equalTo(box.snp.bottom).offset(-20)
            make.leading.equalToSuperview().offset(40)
        }
        addnewButton.addTarget(self, action: #selector(addNewPractice(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    @objc func addNewPractice(_ button:UIButton) {
        let someVIEW = UIView()
        someVIEW.backgroundColor = Colors.blue
        if buttonTag == 0 {
            
        }
        else {
            
//            let stck = UIStackView()
//            let copyView:UIView = box.copyView()
//            stck.addSubview(copyView)
//
//
//            //someVIEW.backgroundColor = Colors.veryDarkGrey
//            mainScroll.addSubview(someVIEW)
//
//            someVIEW.snp.makeConstraints { (make) in
//                make.top.equalTo(box.snp.bottom).offset(20)
//                make.leading.equalTo(20)
//                make.trailing.equalTo(-20)
//                make.height.equalTo(450)
//            }
//
//            someVIEW.addSubview(copyView)
//            copyView.snp.makeConstraints { (make) in
//                make.leading.equalTo(someVIEW.snp.leading).offset(20)
//                make.trailing.equalTo(someVIEW.snp.trailing).offset(-20)
//                make.top.equalTo(box.snp.bottom).offset(30)
//                make.bottom.equalTo(someVIEW.snp.bottom).offset(-10)
//
//            }
//            someVIEW.layoutSubviews()
//
//            //box.layoutSubviews()
//            print("1")
//            buttonTag = 0
        }
        //box.layoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    override func viewDidLayoutSubviews() {
//        dottedBorderView.addDashedBorder()
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

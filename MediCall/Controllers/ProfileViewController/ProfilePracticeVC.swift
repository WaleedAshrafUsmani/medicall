
import UIKit

class ProfilePracticeVC: UIViewController {

    
    // MARK: - Static
    
    
    // MARK: - Class Properties
    
    var timeSlots = [String]()
    var numOfSections = 1
    var showDiscount = false
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var practicesTableView: UITableView!
    
    
    // MARK: - Life Cycle
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        timeSlots.append(String())
        
        viewConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    
    // MARK: - Functions
    
    func viewConfiguration() {
        
        practicesTableView.registerNib(from: WorkPlaceCell.self)
        practicesTableView.registerNib(from: DayCell.self)
        practicesTableView.registerNib(from: FeeCell.self)
        practicesTableView.registerNib(from: DiscountCell.self)
        practicesTableView.registerNib(from: DiscountRateCell.self)
        
        
        
        
    }
    
    
    // MARK: - IBActions

    @IBAction func saveAction(_ sender: UIButton) {
        LoaderManager.show(self.view)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            LoaderManager.hide(self.view)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func addNewSection(_ sender: UIButton) {
        self.numOfSections = self.numOfSections + 1
        self.practicesTableView.reloadData()
    }
}





extension ProfilePracticeVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numOfSections
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 + timeSlots.count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            guard let cell : WorkPlaceCell = tableView.dequeue(cell: WorkPlaceCell.self) else { return UITableViewCell() }
            cell.removedetails.addTarget(self, action: #selector(self.removeSection(_:)), for: .touchDown)
            return cell
            
        }
        else if indexPath.row >= 1 && indexPath.row <= timeSlots.count {
            
            guard let cell : DayCell = tableView.dequeue(cell: DayCell.self) else { return UITableViewCell() }
            if indexPath.row == 1 {
                cell.addRemoveSlot.addTarget(self, action: #selector(self.insertTimeSlot(_:)), for: .touchDown)
            }
            else {
                cell.addRemoveSlot.addTarget(self, action: #selector(self.removeTimeSlot(_:)), for: .touchDown)
            }
            return cell
        }
        else if indexPath.row == timeSlots.count + 1 {
            
            guard let cell : FeeCell = tableView.dequeue(cell: FeeCell.self) else { return UITableViewCell() }
            return cell
        }
        else if indexPath.row == timeSlots.count + 2 {
            
            guard let cell : DiscountCell = tableView.dequeue(cell: DiscountCell.self) else { return UITableViewCell() }
            
            cell.hasDiscount.addTarget(self, action: #selector(showHideDiscount(_:)), for: .valueChanged)
            
            return cell
        }
        else if indexPath.row == timeSlots.count + 3 {
            
            guard let cell : DiscountRateCell = tableView.dequeue(cell: DiscountRateCell    .self) else { return UITableViewCell() }
            
            return cell
        }
        return UITableViewCell()
        
    }
    @objc func insertTimeSlot(_ sender : UIButton) {
        guard let cell = sender.chainedResponderOf(type: DayCell.self) as? DayCell else { return }
        guard var indexPath = self.practicesTableView.indexPath(for: cell) else { return }
        indexPath.row = indexPath.row + 1
        timeSlots.append(String())
        self.practicesTableView.beginUpdates()
        self.practicesTableView.insertRows(at: [indexPath], with: .none)
        self.practicesTableView.endUpdates()
        practicesTableView.reloadData()
        
    }
    @objc func removeTimeSlot(_ sender : UIButton) {
        
        guard let cell = sender.chainedResponderOf(type: DayCell.self) as? DayCell else { return }
        guard let indexPath = self.practicesTableView.indexPath(for: cell) else { return }

        
        self.practicesTableView.beginUpdates()
        self.timeSlots.remove(at: indexPath.row-1)
        self.practicesTableView.deleteRows(at: [indexPath], with: .none)
        self.practicesTableView.endUpdates()    
        
    }
    @objc func showHideDiscount(_ sender : UISwitch) {
        
//        guard let cell = sender.chainedResponderOf(type: DayCell.self) as? DayCell else { return }
//        guard let indexPath = self.practicesTableView.indexPath(for: cell) else { return }
        
        
        
        showDiscount = sender.isOn
        self.practicesTableView.beginUpdates()
        self.practicesTableView.endUpdates()
        
    }
    @objc func removeSection(_ sender : UIButton) {
        
        guard let cell = sender.chainedResponderOf(type: WorkPlaceCell.self) as? WorkPlaceCell else { return }
        guard let indexPath = self.practicesTableView.indexPath(for: cell) else { return }
        
        if numOfSections > 1 {
            self.practicesTableView.beginUpdates()
            self.numOfSections = self.numOfSections - 1
            self.practicesTableView.deleteSections([indexPath.section], with: .none)
            self.practicesTableView.endUpdates()
        }
        else {
            ToastManager.showErrorMessage(message: "You can't delete this section")
        }
        
        
        
    }
}


extension ProfilePracticeVC : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == timeSlots.count + 2 {
            return 45
        }
        else if indexPath.row == timeSlots.count + 3 {
            if showDiscount == true {
                return 60
            }
            else {
                return 0
            }
        }
        else {
            return 35
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = self.practicesTableView.backgroundColor
        return view
    }
    
}


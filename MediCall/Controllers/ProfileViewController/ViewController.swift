
import UIKit
import DropDown
import RSSelectionMenu
import TagListView
class ViewController: UIViewController ,UITextFieldDelegate,TagListViewDelegate{
    let dropDownSpecialist = DropDown()
    let datePicker = UIDatePicker()

    let dropDownLocations = DropDown()
    let dropDownBG = DropDown()
    let simpleDataArray = ["Cardiologist", "Child Specialist", "Medical Specialist", "Dermatologist", "General Surgeon", "Dentist", "Endocrinologist","Plastic Surgeon","Gastroenterologist","Orthopedic Surgeon"]
    var simpleSelectedArray = [String]()

    @IBOutlet weak var DOB: UITextField!
    @IBOutlet weak var TagListView: TagListView!
    @IBOutlet weak var SpecialityTF: UITextField!
    @IBOutlet weak var LocationView: UIView!
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var LocationTF: UITextField!
    @IBOutlet weak var BGTF: UITextField!
    @IBOutlet weak var SpecialistView: UIView!
    @IBOutlet weak var SpecilistTf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        showDatePicker()

        //for Specialist
        dropDownSpecialist.anchorView = SpecialistView // UIView or UIBarButtonItem
        dropDownSpecialist.dataSource = ["Specialist", "Trainee Specialist", "General Practitioner","Student Doctor"]
        //for Specility
        
        
        
        TagListView.removeAllTags()
        
        //for Locations
        dropDownLocations.anchorView = LocationView // UIView or UIBarButtonItem
        dropDownLocations.dataSource = ["Lahore", "Karachi", "FSB","Islambad","RawalPindi","Karachi","Sargodha","Sakhar","Hyedrabad","Quetta","Peshawar","Riwind","Jehlum","Muree"]
        //for BG
        dropDownBG.anchorView = BGView // UIView or UIBarButtonItem
        dropDownBG.dataSource = ["A+ve", "A-ve", "B+ve","B-ve","O+ve","O-ve","AB+ve","AB-ve"]
        
        dropDownSpecial()
        dropDownSpecialiti()
        dropDownLocation()
        dropDownBloodGroup()

        // Do any additional setup after loading the view.
    }
    func dropDownSpecial()
    {
            dropDownSpecialist.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.SpecilistTf.text = item
            self.dropDownSpecialist.hide()
        }
        
        // Will set a custom width instead of the anchor view width
        dropDownSpecialist.width = 200
        dropDownSpecialist.direction = .bottom

        
    }
    

    func dropDownSpecialiti()
    {
    
        let selectionMenu =  RSSelectionMenu(selectionType: .Multiple,dataSource: simpleDataArray) { (cell, object, indexPath) in
            cell.textLabel?.text = object
            
            // Change tint color (if needed)
            cell.tintColor = .orange
        }
        
        // set default selected items when menu present on screen.
        // Here you'll get onDidSelectRow
        //self.simpleSelectedArray.removeAll()

        selectionMenu.setSelectedItems(items: simpleSelectedArray) { (text, isSelected, selectedItems) in
            
            // update your existing array with updated selected items, so when menu presents second time updated items will be default selected.
            
            
            self.simpleSelectedArray = selectedItems
            self.TagListView.removeAllTags()
            self.TagListView.addTags(self.simpleSelectedArray)
            
                
                
            
            
            //self.SpecialityTF.text = self.simpleSelectedArray.popLast()
           }

        // auto dismiss
        selectionMenu.dismissAutomatically = false      // default is true
        
        // show as PresentationStyle = Push
        selectionMenu.show(style: .Push, from: self)
        
    }
    func dropDownLocation()
    {
        dropDownLocations.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.LocationTF.text = item
            self.dropDownLocations.hide()
        }
        
        // Will set a custom width instead of the anchor view width
        dropDownLocations.width = 200
        dropDownLocations.direction = .bottom
        
        
    }
    
    func dropDownBloodGroup()
    {
        dropDownBG.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.BGTF.text = item
            self.dropDownBG.hide()
        }
        
        // Will set a custom width instead of the anchor view width
        dropDownBG.width = 200
        dropDownBG.direction = .bottom
        
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            if textField == SpecilistTf
            {
            dropDownSpecialist.show()
            textField.endEditing(true)
           }
           if textField == SpecialityTF
           {
            
            dropDownSpecialiti()
            textField.endEditing(true)
            
            }
            if textField == LocationTF
            {
            dropDownLocations.show()
            textField.endEditing(true)
            }
            if textField == BGTF
            {
            dropDownBG.show()
            textField.endEditing(true)
            }
                
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func Save(_ sender: UIButton) {
        LoaderManager.show(self.view)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 2) {
            LoaderManager.hide(self.view)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        
        //done button & cancel button
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(ViewController.donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(ViewController.cancelDatePicker))
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        // add toolbar to textField
        DOB.inputAccessoryView = toolbar
        // add datepicker to textField
        DOB.inputView = datePicker
        
    }
    
    func donedatePicker(){
        //For date formate
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        DOB.text = formatter.string(from: datePicker.date)
        //dismiss date picker dialog
        self.view.endEditing(true)
    }
    
    func cancelDatePicker(){
        //cancel button dismiss datepicker dialog
        self.view.endEditing(true)
    }

}

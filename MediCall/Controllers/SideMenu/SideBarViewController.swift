
import UIKit

struct STORYBOARD {
    static let LOGIN = "LoginStoryboard"
}
class SideBarViewController: UIViewController {
    
    @IBOutlet weak var profileImageView: UIStackView!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var loginBtn: UIButton!
    
    @IBOutlet weak var defaultProfileImage: UIImageView!
    
    @IBOutlet weak var CreateAccountBtn: UIButton!
    @IBOutlet weak var createAccountStack: UIStackView!
    @IBOutlet weak var logoutStack: UIStackView!
    var auth = 0
    override func viewDidLoad() {
        super.viewDidLoad()
//        gradientView.setGradientBackground(colorOne:Colors.first, colorTwo: Colors.second, colorThree: Colors.third)
//        gradientView.insertSubview(profileImageView,aboveSubview:gradientView)
        gradientView.backgroundColor = .clear
        loginBtn.contentHorizontalAlignment = .left
        if let userName = UserDefaultManager.shared.readUser().userFullName
        {
            if userName.isNotEmpty {
                CreateAccountBtn.setTitle("Free Consultation", for: .normal)
            loginBtn.setTitle(userName, for: .normal)
            auth = 1
        }
        }
//        let imageUrl = "http://themedicall.com/public/uploaded/img/specialities/" + dataSourceArray[indexPath.row].speciality_icon!
//
//        let url = URL(string: imageUrl)
//        cell.findDoctorImage.af_setImage(withURL: url!)
         let userImage = UserDefaultManager.shared.readUser().profileImage
        
             let imageUrl = "http://themedicall.com/public/uploaded/img/doctors/" + (userImage ?? "")
            print(imageUrl)
            let url = URL(string: imageUrl)
            defaultProfileImage.af_setImage(withURL: url!)
        
        if auth == 1
        {
            loginBtn.addTarget(self, action: #selector(freeConsultation(_:)), for: .touchUpInside)
            
        }
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaultManager.shared.readUser().userName != nil {
            loginBtn.setTitle(UserDefaultManager.shared.readUser().userName, for: .normal)
            loginBtn.isUserInteractionEnabled = true
            logoutStack.isHidden = false
            createAccountStack.isHidden = true
        }
        else {
            loginBtn.setTitle("Log in", for: .normal)
            loginBtn.isUserInteractionEnabled = true
            logoutStack.isHidden = true
            createAccountStack.isHidden = false
        }
    }
    
    @objc func freeConsultation(_ button:UIButton) {
        print("free free free")
        button.dismiss()
        
        self.performSegue(withIdentifier: "SignUp", sender:self)
    }
    
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if auth == 1 {
            return false
        }
        return true
    }
  
    @IBAction func touchButtonAction(_ sender: UIButton) {
        self.view.addTapToDismiss()
    }
    
       
    @IBAction func LogOutBtnAction(_ sender: UIButton) {
        UserDefaultManager.shared.removeUser()
//        self.performSegue(withIdentifier: "mainView", sender:self)
        sideMenuController?.hideMenu()
    }
    
    @IBAction func showHome(_ sender: Any) {
        sideMenuController?.hideMenu()
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        
        if UserDefaultManager.shared.readUser().userName != nil {
            performSegue(withIdentifier: "ShowProfile", sender: self)
        }
        else {
            let loginController = lGViewController.instantiateFromStoryboard(STORYBOARD.LOGIN)
            self.present(to: loginController)
        }
        
        
        
        
    }
    @IBAction func actionCreateAccount(_ sender: Any) {
        let loginController = lGViewController.instantiateFromStoryboard(STORYBOARD.LOGIN)
        self.present(to: loginController)
    }
    
    func present(to viewController: UIViewController) {
        viewController.modalTransitionStyle = .coverVertical
        sideMenuController?.hideMenu()
        let toBePresent = UINavigationController(rootViewController: viewController)
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        toBePresent.navigationBar.titleTextAttributes = textAttributes
        self.present(toBePresent, animated: true, completion: nil)
    }

}

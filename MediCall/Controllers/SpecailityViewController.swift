
import UIKit


protocol SpecialitySelectionDelegate {
    func didSelectSpeciality(speciality:[String]?)
}

class SpecailityViewController: UIViewController
{
 var specialitySelectionDelegate:SpecialitySelectionDelegate?
    
    var arr = ["Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist","Specialist","ChildSpecialist"]
    var apeendingArray = [String]()
    var isChecked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
       
        
    }
    
    @IBAction func doneBtnAction(_ sender: UIButton) {
        print(apeendingArray)
        specialitySelectionDelegate?.didSelectSpeciality(speciality:apeendingArray)
        
        self.dismiss(animated: true, completion: nil)
    }
    

    

}

extension SpecailityViewController:UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "special", for: indexPath) as! SpecialityTableViewCell
        cell.specialityLbl.text = arr[indexPath.row]
        if cell.specialityImage.image == #imageLiteral(resourceName: "unchecked-1") {
            if isChecked {
                cell.specialityImage.image = #imageLiteral(resourceName: "checked-1")
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        apeendingArray.append(arr[indexPath.row])
        //specialitySelectionDelegate?.didSelectSpeciality(speciality: arr[indexPath.row])
        
    }
}

extension SpecailityViewController:UITableViewDelegate {
    
}


import UIKit
import Alamofire


enum UserType : Int {
    
    case TraineeSpecialist = 0, GeneralPractitioner, StudentDoctor
    
}


class UserFormViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var userNameTextfield: UITextField!
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var pmdcTextField: UITextField!
    @IBOutlet weak var cameraImage: UIImageView!
    @IBOutlet weak var galleryImage: UIImageView!
    @IBOutlet weak var FemaleRadioImage: UIImageView!
    @IBOutlet weak var maleRadioImage: UIImageView!
    @IBOutlet weak var malepopUpButton: UIButton!
    @IBOutlet weak var femalePopUpButton: UIButton!
    @IBOutlet weak var statusTextField: UITextField!
    @IBOutlet weak var dottedBorderView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var cameraViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var extraTextField: UITextField!
    @IBOutlet weak var extraFieldConstraint: NSLayoutConstraint!
    
    var statusId = 1
    var gender = ""
    let picker = UIDatePicker()
    let listPicker = UIPickerView()
    var userAuth = 0
    var validUser:UserValidation?
    var singUp = RegisterDoctor()
    var cityofId = 0
    
    var status = ["Trainee Specialist","General Practitioner","Student Doctor"]
    
    var userType : UserType!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        userType = UserType.TraineeSpecialist
        self.statusTextField.text = "Trainee Specialist"
        changeViewAccordingtoStatus(selectedUser: 0)
        cityLbl.text = "Lahore"
        statusTextField.inputView = listPicker
        listPicker.delegate = self
        listPicker.dataSource = self
        //phoneTextField.delegate = self
        userNameTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: UIControlEvents.editingDidEnd)
        phoneTextField.addTarget(self, action: #selector(textFieldDidBegin(_:)), for:UIControlEvents.editingChanged)
        
        let yourViewBorder = CAShapeLayer()
        yourViewBorder.strokeColor = UIColor.black.cgColor
        yourViewBorder.lineDashPattern = [3, 3]
        yourViewBorder.frame = dottedBorderView.bounds
        yourViewBorder.fillColor = nil
        yourViewBorder.path = UIBezierPath(rect: dottedBorderView.bounds).cgPath
        dottedBorderView.layer.addSublayer(yourViewBorder)
        
        
        malepopUpButton.addTarget(self, action: #selector(handleCheck(_:)), for: .touchUpInside)
         femalePopUpButton.addTarget(self, action: #selector(handleCheck(_:)), for: .touchUpInside)
       self.view.addTapToDismiss()
        createDatePicker()
        // Do any additional setup after loading the view.
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        validateUserFromApi(textField)
        
    }
    
    @objc func textFieldDidBegin(_ textField: UITextField) {
        if textField.text?.first != "3"
        {
            let myAlert = UIAlertController(title: "Warning!", message: " Phone number must start from three", preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
                        { action in
                            self.phoneTextField.text = ""
                            //self.dismiss(animated: true , completion: nil);
            
                        }
                        myAlert.addAction(okAction);
                        phoneTextField.text = ""
            
                        self.present(myAlert, animated: true, completion: nil)
            
        }
        else {
            return
        }
        
    }
    

    
     func showError() {
        if validUser?.Error == true {
            ToastManager.showErrorMessage(message:(validUser?.errorMessage!)!)
        }
    }
    
    func validateUserFromApi(_ textfield:UITextField) {
        let username = textfield.text
        let apiCall = ApiManager.shared.isUserValid(userName: username!)
        LoaderManager.show(self.view)
        apiCall.then {
            valid -> Void in
            LoaderManager.hide(self.view)
            if valid.Error == true {
                self.validUser = valid
                self.showError()
            }
            
            }.catch {
                error -> Void in
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    fileprivate func validateTraineeSpecialist() -> (Bool, String) {
        
        if userNameTextfield.text?.isEmpty ?? true {
            return (false, "Please enter user name")
        }
        else if fullNameTextField.text?.isEmpty ?? true {
            return (false, "Please enter fullName")
        }
        else if phoneTextField.text?.isEmpty ?? true {
            return (false, "Please enter Phone#")
        }
            
        else if phoneTextField.text?.count != 10 {
            return (false, "Please enter valid number")
        }
        else if dobTextField.text?.isEmpty ?? true {
            return (false, "Please enter DOB")
        }
        else if emailTextField.text?.isEmpty ?? true {
            return (false, "Please enter Email")
        }
            
        else if emailTextField.text?.isValidEmail() != true{
            return (false, "Enter Valid Email")
        }
        else if passwordTextField.text?.isEmpty ?? true {
            return (false, "Please enter password")
        }
        else if confirmPasswordTextField.text?.isEmpty ?? true
        {
            return (false, "Please enter Repassword")
        }
        else if passwordTextField.text != confirmPasswordTextField.text {
            return (false, "Password doesn't match")
        }
            
        else if pmdcTextField.text?.isEmpty ?? true {
            return (false, "Please enter Course Registration #")
        }
        else {
            return (true, "")
        }
        
    }
    fileprivate func validateGeneralPractitioner() -> (Bool, String) {
        
        if userNameTextfield.text?.isEmpty ?? true {
            return (false, "Please enter user name")
        }
        else if fullNameTextField.text?.isEmpty ?? true {
            return (false, "Please enter fullName")
        }
        else if phoneTextField.text?.isEmpty ?? true {
            return (false, "Please enter Phone#")
        }
            
        else if phoneTextField.text?.count != 10 {
            return (false, "Please enter valid number")
        }
        else if dobTextField.text?.isEmpty ?? true {
            return (false, "Please enter DOB")
        }
        else if emailTextField.text?.isEmpty ?? true {
            return (false, "Please enter Email")
        }
            
        else if emailTextField.text?.isValidEmail() != true{
            return (false, "Enter Valid Email")
        }
        else if passwordTextField.text?.isEmpty ?? true {
            return (false, "Please enter password")
        }
        else if confirmPasswordTextField.text?.isEmpty ?? true
        {
            return (false, "Please enter Repassword")
        }
        else if passwordTextField.text != confirmPasswordTextField.text {
            return (false, "Password doesn't match")
        }
            
        else if pmdcTextField.text?.isEmpty ?? true {
            return (false, "Please PMDC number")
        }
        else {
            return (true, "")
        }
        
    }
    
    fileprivate func validateStudentDoctor() -> (Bool, String) {
        
        if userNameTextfield.text?.isEmpty ?? true {
            return (false, "Please enter user name")
        }
        else if fullNameTextField.text?.isEmpty ?? true {
            return (false, "Please enter fullName")
        }
        else if phoneTextField.text?.isEmpty ?? true {
            return (false, "Please enter Phone#")
        }
            
        else if phoneTextField.text?.count != 10 {
            return (false, "Please enter valid number")
        }
        else if dobTextField.text?.isEmpty ?? true {
            return (false, "Please enter DOB")
        }
        else if emailTextField.text?.isEmpty ?? true {
            return (false, "Please enter university name")
        }
            
        else if passwordTextField.text?.isEmpty ?? true {
            return (false, "Please university registration #")
        }
        else if confirmPasswordTextField.text?.isEmpty ?? true
        {
            return (false, "Please enter email")
        }
        else if confirmPasswordTextField.text?.isValidEmail() != true{
            return (false, "Enter Valid Email")
        }
        
            
        else if pmdcTextField.text?.isEmpty ?? true {
            return (false, "Please enter password")
        }
        else if extraTextField.text?.isEmpty ?? true {
            return (false, "Please enter confirm password")
        }
        else if pmdcTextField.text != extraTextField.text {
            return (false, "Password doesn't match")
        }
        else {
            return (true, "")
        }
        
    }
    
    
    
    
    @IBAction func signUPBtnAction(_ sender: UIButton) {
       
        
        switch userType! {
        case .TraineeSpecialist:
            let (success, errorMessage) = validateTraineeSpecialist()
            if !success {
                ToastManager.showErrorMessage(message: errorMessage)
                return
            }
            break
        case .GeneralPractitioner:
            let (success, errorMessage) = validateGeneralPractitioner()
            if !success {
                ToastManager.showErrorMessage(message: errorMessage)
                return
            }
            break
        case .StudentDoctor:
            let (success, errorMessage) = validateStudentDoctor()
            if !success {
                ToastManager.showErrorMessage(message: errorMessage)
                return
            }
            break
        }
        
        
        
        
        
        
        

        
        ApiManager.shared.trySignup(with: paramsForRegistration(), success: { [weak self] (doctor) in
            
            guard let weakSelf = self else { return }
            
            weakSelf.singUp = doctor
            weakSelf.showSignUp(sender)
            var user = User()
            user.userName = "Umer1234"
            UserDefaultManager.shared.writeUser(user)
            
        }) { (error) in
            
            ToastManager.showErrorMessage(message: error!.localizedDescription)
        }
        
    }
    func paramsForRegistration() -> [String : Any] {
        
        
        var params : Parameters!
        
        if userType == UserType.TraineeSpecialist {
            
            params =
                [
                    "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                    "username" : userNameTextfield.text!,
                    "fullname" : fullNameTextField.text!,
                    "mobilenumber" : phoneTextField.text!,
                    "dob" : dobTextField.text!,
                    "city" : cityofId,
                    "gender" : gender,
                    "email" : emailTextField.text!,
                    "password" : passwordTextField.text!,
                    "experience_status_id" : statusId,
                    "pmdc_number" : pmdcTextField.text!,
                    "social_id" : ""
            ]
            
        }
        else if userType == UserType.GeneralPractitioner {
            
            params =
                [
                    "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                    "username" : userNameTextfield.text!,
                    "fullname" : fullNameTextField.text!,
                    "mobilenumber" : phoneTextField.text!,
                    "dob" : dobTextField.text!,
                    "city" : cityofId,
                    "gender" : gender,
                    "email" : emailTextField.text!,
                    "password" : passwordTextField.text!,
                    "experience_status_id" : statusId,
                    "pmdc_number" : pmdcTextField.text!,
                    "social_id" : ""
            ]
            
        }
        else if userType == UserType.StudentDoctor {
            
            params =
                [
                    "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                    "username" : userNameTextfield.text!,
                    "fullname" : fullNameTextField.text!,
                    "mobilenumber" : phoneTextField.text!,
                    "dob" : dobTextField.text!,
                    "city" : cityofId,
                    "gender" : gender,
                    "uni_name" : emailTextField.text!,
                    "uni_registraion" : passwordTextField.text!,
                    "experience_status_id" : "4",
                    "email" : confirmPasswordTextField.text!,
                    "password" : pmdcTextField.text!,
                    "social_id" : ""
            ]
            
        }
        return params
        
    }
    
    func showSignUp(_ button:UIButton) {

        
//       self.performSegue(withIdentifier: "verifyDoctor", sender:button)
//        self.navigationController?.popToRootViewController(animated: false)
//        self.navigationController?.dismiss(animated: false, completion: nil)
        self.performSegue(withIdentifier: "tempSignup", sender:button)
        
//        let viewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateInitialViewController()
//        AppDelegate().window?.rootViewController = viewController
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let ctrl = segue.destination as? VerifyStatusViewController {
            ctrl.regDoctor = singUp
        }
    }
    
    
    @IBAction func cityButtonAction(_ sender: UIButton) {
//        let popupViewControllerIdentifier = "popupViewController"
//        let main = UIStoryboard(name: "Main", bundle: nil)
//        let controller = main.instantiateViewController(withIdentifier: "popUP") as! PopUpViewController
//        controller.modalTransitionStyle = .crossDissolve
//        controller.modalPresentationStyle = .overFullScreen
//        print("clicked")
//        self.present(controller, animated: false, completion: nil)
    }
    @objc func handleCheck(_ sender:UIButton) {
        if sender.tag == 0 {
            maleRadioImage.image = #imageLiteral(resourceName: "radio_btn_fill")
            FemaleRadioImage.image = #imageLiteral(resourceName: "radio_btn_unfill")
            gender = "Male"
            print("Male")
            
        }
        if sender.tag == 1 {
            FemaleRadioImage.image = #imageLiteral(resourceName: "radio_btn_fill")
            maleRadioImage.image = #imageLiteral(resourceName: "radio_btn_unfill")
            gender = "Female"
            print("Female")
        }
    }
    
    @IBAction func dobTextfieldAction(_ sender: UITextField) {
        let datePickerView  = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: Selector(("handleDatePicker:")), for: UIControlEvents.touchUpInside)
        
    }
    func handleDatePicker(sender: UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        dobTextField.text = timeFormatter.string(from: sender.date)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
//        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
//        galleryImage.image = image
//        galleryImage.contentMode = .scaleAspectFit
        if #available(iOS 11.0, *) {
            if let imgUrl = info[UIImagePickerControllerImageURL] as? URL
            {
                let imgName = imgUrl.lastPathComponent
                let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
                let localPath = documentDirectory?.appending(imgName)
                
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                let data = UIImagePNGRepresentation(image)! as NSData
                data.write(toFile: localPath!, atomically: true)
                //let imageData = NSData(contentsOfFile: localPath!)!
                let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!
                
               // print("the path of photo is", photoURL)
                
                let a = "http://themedicall.com/medi-app-api/upload-doctor-pmdc-picture"
                // define parameters
               let parameters: Parameters = [
                    "doctor_id":"14445",
                    "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                    "pmdc_picture" : photoURL
                    ]
                requestWith(endUrl: a, parameters: parameters, imageUrl: photoURL)
                
                
//
//                let headers = [
//                    "content-type": "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
//                    "Cache-Control": "no-cache",
//                    "Postman-Token": "502472c9-9e91-4e87-9d85-ae8293a844dd"
//                ]
//                let parameters = [
//                    [
//                        "name": "pmdc_picture",
//                        "fileName": "/Users/macbook/Desktop/4.jpg"
//                    ],
//                    [
//                        "name": "key",
//                        "value": "Vx0cbjkzfQpyTObY8vfqgN1us"
//
//
//                    ],
//                    [
//                        "name": "doctor_id",
//                        "value": "14445"
//                    ]
//                ]
//
//                let boundary = "----WebKitFormBoundary7MA4YWxkTrZu0gW"
//
//                var body = ""
//                var error: NSError? = nil
//                for param in parameters {
//                    let paramName = param["name"]!
//                    body += "--\(boundary)\r\n"
//                    body += "Content-Disposition:form-data; name=\"\(paramName)\""
//                    if let filename = param["fileName"] {
//                        let contentType = param["content-type"]!
//                        let fileContent = try! String(contentsOfFile: filename, encoding: String.Encoding.utf8)
//                        if (error != nil) {
//                            print(error!)
//                        }
//                        body += "; filename=\"\(filename)\"\r\n"
//                        body += "Content-Type: \(contentType)\r\n\r\n"
//                        body += fileContent
//                    } else if let paramValue = param["value"] {
//                        body += "\r\n\r\n\(paramValue)"
//                    }
//                }
//
//                let request = NSMutableURLRequest(url: NSURL(string: "http://themedicall.com/medi-app-api/upload-doctor-pmdc-picture")! as URL,
//                                                  cachePolicy: .useProtocolCachePolicy,
//                                                  timeoutInterval: 10.0)
//                request.httpMethod = "POST"
//                request.allHTTPHeaderFields = headers
//                request.httpBody = postData as Data
//
//                let session = URLSession.shared
//                let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
//                    if (error != nil) {
//                        print(error!)
//                    } else {
//                        let httpResponse = response as? HTTPURLResponse
//                        print(httpResponse!)
//                    }
//                })
//
//                dataTask.resume()
                
                
                galleryImage.image = image
                galleryImage.contentMode = .scaleAspectFit
                
                
            }
        } else {
            // Fallback on earlier versions
        }
    
        
        dismiss(animated: true, completion: nil)
    }
    
    func requestWith(endUrl: String, parameters: [String : Any],imageUrl:URL){
        
        let url = endUrl    /* your API url */
        
        let headers: HTTPHeaders = [
            /* "Authorization": "your_access_token",  in case you need authorization header */
            "Content-type": "multipart/form-data"
            ]
        
        Alamofire.upload(
            multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
         
               // multipartFormData.append(imageUrl, withName: "image", fileName: "", mimeType: "image/png")
                //multipartFormData.append(imageUrl, withName: "Some File")
                //multipartFormData.append(fileURL: imageUrl, name: "photo")
                multipartFormData.append(imageUrl, withName: "some")
                
            
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    print(response.result.value)
                    print(response.result.description)
                    
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }

    
    @IBAction func galleryButtonAction(_ sender: UIButton) {
        print("some")
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .photoLibrary
        present(controller, animated: true, completion: nil)
    }
    
     @IBAction func cameraButtonAction(_ sender: UIButton) {
        print("some")
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .camera
        present(controller, animated: true, completion: nil)
     }
    
    func createDatePicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([done], animated: false)
        
        dobTextField.inputAccessoryView = toolBar
        dobTextField.inputView = picker
        
        picker.datePickerMode = .date
        picker.maximumDate = Date()
        
        picker.setValue(UIColor.black, forKeyPath: "textColor")
        
    }
    @objc func donePressed(){
        
        print(picker.date)
        let datestring = "\(picker.date)"
        print(datestring)
        let first10 = datestring.substring(to:(datestring.index((datestring.startIndex), offsetBy: 10)))
        dobTextField.text = first10
        dobTextField.textColor = UIColor.black
        self.view.endEditing(true)
        
        
    }
    
    func listpicker(){
        
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let done = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolBar.setItems([done], animated: false)
        
        //statusTextField.inputAccessoryView = toolBar
        statusTextField.inputView = listPicker
        
        
        
//        listPicker = .date
//        listPicker.maximumDate = Date()
        
        picker.setValue(UIColor.black, forKeyPath: "textColor")
        
    }
    
   
    @IBAction func citySelectionBtnAction(_ sender: UIButton) {
        let main = UIStoryboard(name: "Main", bundle: nil)
        let selectionVC = main.instantiateViewController(withIdentifier: "popUP") as? PopUpViewController
        selectionVC?.citySelectionDelegate = self as? CitySelectionDelegate
        present(selectionVC!, animated: true, completion: nil)
        
    }
    
    

}

extension UserFormViewController:UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
       return status[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
     return status.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        statusTextField.text = status[row]
        statusId += row
        changeViewAccordingtoStatus(selectedUser: row)
        
    }
    func changeViewAccordingtoStatus(selectedUser : Int) {
       
        switch selectedUser {
        case 0:
            userType = UserType.TraineeSpecialist
            self.emailTextField.placeholder = "Email"
            self.passwordTextField.placeholder = "Password"
            self.confirmPasswordTextField.placeholder = "Confirm Password"
            self.pmdcTextField.placeholder = "Counsel Registration #"
            cameraViewConstraint.constant = 0
            self.dottedBorderView.isHidden = true
            self.extraFieldConstraint.constant = 0
            self.extraTextField.isHidden = true
            
            self.passwordTextField.isSecureTextEntry = true
            self.confirmPasswordTextField.isSecureTextEntry = true
            self.pmdcTextField.isSecureTextEntry = false
            self.extraTextField.isSecureTextEntry = false
            break
        case 1:
            userType = UserType.GeneralPractitioner
            self.emailTextField.placeholder = "Email"
            self.passwordTextField.placeholder = "Password"
            self.confirmPasswordTextField.placeholder = "Confirm Password"
            self.pmdcTextField.placeholder = "PMDC"
            cameraViewConstraint.constant = 100
            self.dottedBorderView.isHidden = false
            self.extraFieldConstraint.constant = 0
            self.extraTextField.isHidden = true
            
            self.passwordTextField.isSecureTextEntry = true
            self.confirmPasswordTextField.isSecureTextEntry = true
            self.pmdcTextField.isSecureTextEntry = false
            self.extraTextField.isSecureTextEntry = false
            
            break
        case 2:
            userType = UserType.StudentDoctor
            self.emailTextField.placeholder = "University Name"
            self.passwordTextField.placeholder = "University registration #"
            self.confirmPasswordTextField.placeholder = "Email"
            self.pmdcTextField.placeholder = "Password"
            self.extraTextField.placeholder = "Confirm Password"
            self.extraFieldConstraint.constant = 35
            cameraViewConstraint.constant = 100
            self.extraTextField.isHidden = false
            self.dottedBorderView.isHidden = false
            
            self.passwordTextField.isSecureTextEntry = false
            self.confirmPasswordTextField.isSecureTextEntry = false
            self.pmdcTextField.isSecureTextEntry = true
            self.extraTextField.isSecureTextEntry = true
            break
        default:
            break
        }
        
    }
    
    
}

extension UserFormViewController:CitySelectionDelegate {
    func didSelectCity(city: String?, cityId: Int?) {
        cityLbl.text = city
        cityofId = cityId!
        
    }
    
   
    
    
}

//extension UserFormViewController:UITextFieldDelegate {
//
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if phoneTextField.text?.first?.description == "3"
//        {
//
//            guard let text = phoneTextField.text else { return true }
//            let newLength = text.count + string.count - range.length
//            return newLength <= 11
//
//        }
//        else
//        {
//            let myAlert = UIAlertController(title: "Warning!", message: " Phone number must start from zero", preferredStyle: UIAlertControllerStyle.alert)
//            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
//            { action in
//                self.phoneTextField.text = ""
//                //self.dismiss(animated: true , completion: nil);
//
//            }
//            myAlert.addAction(okAction);
//            phoneTextField.text = ""
//
//            self.present(myAlert, animated: true, completion: nil)
//            return true
//        }
//
//        }

   

        
    
        
//}











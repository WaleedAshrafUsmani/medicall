
import UIKit

class VerifyStatusViewController: UIViewController {

    @IBOutlet weak var successLbl: UILabel!
    @IBOutlet weak var congLbl: UILabel!
    @IBOutlet weak var enterPinTextField: UITextField!
    @IBOutlet weak var reSendCode: UIButton!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var updateLaterBtn: UIButton!
    @IBOutlet weak var updateProfileBtn: UIButton!
    
    @IBOutlet weak var verifyBtnView: UIView!
    var regDoctor = RegisterDoctor()
    var verifiedUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.setAnimationsEnabled(false)
        self.performSegue(withIdentifier: "mainView", sender:self.verifyButton)
        UIView.setAnimationsEnabled(true)
        congLbl.alpha = 0.0
        successLbl.alpha = 0.0
        updateLaterBtn.alpha = 0.0
        updateProfileBtn.alpha = 0.0
        showCode()
        
        verifyButton.addTarget(self, action: #selector(verifyUser(_:)), for: .touchUpInside)
        updateLaterBtn.addTarget(self, action: #selector(showSegue(_:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    func showCode() {
        //print(regDoctor.Code)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func verifyUser(_ button:UIButton) {
       
        //let Code = "\(String(describing: regDoctor.Code!))"
        
       // print(Code)
       // if enterPinTextField.text == Code {
            let apiCall = ApiManager.shared.verifyUser(userId:987)
            LoaderManager.show(self.view)
            apiCall.then {
                
                user -> Void in
                LoaderManager.hide(self.view)
                self.verifiedUser = user
                UserDefaultManager.shared.writeUser(self.verifiedUser)
                
                
                self.enterPinTextField.isHidden = true
                self.reSendCode.isHidden = true
                self.verifyButton.isHidden = true
                self.verifyBtnView.isHidden = true
                //self.congLbl.isHidden = false
                //self.successLbl.isHidden = false
               // self.updateLaterBtn.isHidden = false
              //  self.updateProfileBtn.isHidden = false
                
                UIView.animate(withDuration: 6.0, animations: {
                    self.updateLaterBtn.alpha = 1.0
                    self.updateProfileBtn.alpha = 1.0
                    self.congLbl.alpha = 1.0
                    self.successLbl.alpha = 1.0
                    self.updateProfileBtn.width = 70
                    self.updateLaterBtn.width = 70
                    
                })
                { (finished) in
                   // fade out
                    UIView.animate(withDuration: 4.0, animations: {
                        
                    })
                }
                

            
            }
            
            
            
        //}
//        else {
//            ToastManager.showToastMessage(message: "Code Doesnot match")
//        }
    }
    @objc func showSegue(_ button:UIButton) {
        self.performSegue(withIdentifier: "mainView", sender:button)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

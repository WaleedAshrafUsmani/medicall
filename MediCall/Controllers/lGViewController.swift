
import UIKit
import Foundation

class lGViewController: MCOverlayViewController {

    
    lazy var user = User()
    @IBOutlet weak var mainStackView: UIStackView!
    
    @IBOutlet weak var googleStackView:UIStackView!
    @IBOutlet weak var facebookStackView:UIStackView!
    @IBOutlet weak var userNameView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Login"
        let myView = UIView()
        myView.backgroundColor = Colors.green
        myView.setBorderColor(color: .black)
        myView.setCornerRadius(r: 20)
        googleStackView.addSubview(myView)
    }
    
    @IBAction func loginActionButton(_ sender: UIButton) {
        if userNameTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: ERROR.invalidUserName)
            return
        }
        if passwordTextField.text?.isEmpty ?? true {
            ToastManager.showErrorMessage(message: ERROR.invalidPassword)
            return
        }
        makeLoginApi()
        showUserName()
    }
    
    func makeLoginApi() {
        let email = userNameTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        LoaderManager.show(self.view)
        
        let apiCall = ApiManager.shared.loginUser2(email:email , password: password)
        apiCall.then {
            user -> Void in
               LoaderManager.hide(self.view)
            if let error = user.error {
                if error {
                    ToastManager.showErrorMessage(message: "\(user.errorMessage ?? "" )")
                    return;
                }
            }
            
            self.user = user
            UserDefaultManager.shared.writeUser(self.user)
            self.dismisss()
            
            }.catch { (error) in
                ToastManager.showErrorMessage(message: error.localizedDescription)
        }
    }
    
    func showUserName() {
        print("\(user.userFullName ?? "No user name")")
    }

}


import UIKit

extension Dictionary {
    func stringFromHttpParameters() -> String {
        let parameterArray = self.map { (key, value) -> String in
            let percentEscapedKey = (key as! String)
            let percentEscapedValue = (value as AnyObject).description as String
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }
        if parameterArray.count == 0 { return "" }
        return "?"+parameterArray.joined(separator: "&")
    }
}

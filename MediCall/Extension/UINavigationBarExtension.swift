

import Foundation
import UIKit


extension UINavigationBar {
    
    func configureForBlueBackground() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.barTintColor = UIColor.init(red: 17.0/255.0, green: 42.0/255.0, blue: 78.0/255.0, alpha: 1.0)
        self.tintColor = .white
        self.isTranslucent = false
    }
}


extension UIResponder {
    
    func chainedResponderOf<T: Any>(type responderClassType: T) -> UIResponder? {
        
        guard let classType = responderClassType as? AnyClass else { return nil }
        
        var responder: UIResponder? = self.next
        
        while responder != nil {
            
            if responder!.isKind(of: classType) {
                return responder!
            }
            responder = responder!.next
        }
        
        return nil
    }
}


import UIKit
import Foundation

extension UITableView {

    func setAndLayoutTableHeaderView(header: UIView) {
        self.tableHeaderView = header
        header.setNeedsLayout()
        header.layoutIfNeeded()
        let height = header.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        var frame = header.frame
        frame.size.height = height
        header.frame = frame
        self.tableHeaderView = header
    }
    
    func reloadRowsInSection(section: Int, oldCount:Int, newCount: Int){
        
        let maxCount = max(oldCount, newCount)
        let minCount = min(oldCount, newCount)
        
        var changed = [NSIndexPath]()
        
        for i in minCount..<maxCount {
            let indexPath = NSIndexPath(item: i, section: section)
            changed.append(indexPath)
        }
        
        var reload = [NSIndexPath]()
        for i in 0..<minCount{
            let indexPath = NSIndexPath(item: i, section: section)
            reload.append(indexPath)
        }
        
        beginUpdates()
        if(newCount > oldCount) {
            insertRows(at: changed as [IndexPath], with: .fade)
        }
        else if(oldCount > newCount){
            deleteRows(at: changed as [IndexPath], with: .fade)
        }
        
        if(newCount > oldCount || newCount == oldCount) {
            reloadRows(at: reload as [IndexPath], with: .bottom)
        }
        endUpdates()
        
    }
}


extension UITableView {
    
    func registerNib(from cellClass: UITableViewCell.Type) {
        let identifier = cellClass.identifier
        register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
    
//    func registerNib(from headerFooterClass: UITableViewHeaderFooterView.Type) {
//        let identifier = headerFooterClass.identifier
//        register(UINib(nibName: identifier, bundle: nil), forHeaderFooterViewReuseIdentifier: identifier)
//    }
    
    
    
    
    
    func registerCell(from cellClass: UITableViewCell.Type) {
        register(cellClass, forCellReuseIdentifier: cellClass.identifier)
    }
    
    func dequeue<T: Any>(cell: UITableViewCell.Type) -> T? {
        return dequeueReusableCell(withIdentifier: cell.identifier) as? T
    }
    
    func dequeue<T: Any>(headerFooter: UITableViewHeaderFooterView.Type) -> T? {
        
        return dequeueReusableHeaderFooterView(withIdentifier: headerFooter.identifier) as? T
    }
    
    func reloadTableWithoutScrolling() {
        
        let contentOffset = self.contentOffset
        self.reloadData()
        self.layoutIfNeeded()
        self.setContentOffset(contentOffset, animated: false)
    }
    
}






extension UITableView {
    
    func addRefreshControl(_ refresher: UIRefreshControl, withSelector selector:Selector) {
        
        refresher.addTarget(nil, action: selector, for: .valueChanged)
        if #available(iOS 10.0, *) {
            refreshControl = refresher
        } else {
            addSubview(refresher)
        }
    }
    
}

extension NSObjectProtocol {
    
    static var identifier: String { return String(describing: self) }
}

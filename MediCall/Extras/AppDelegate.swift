
import UIKit
import Alamofire
import IQKeyboardManagerSwift
import SideMenuSwift
import GoogleMaps
import GooglePlaces
import DropDown
@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, SplashScreenDelegate {

    var window: UIWindow?
    
    static var menu_Bool = true
    struct MyVariables {
        static var yourVariable = ""
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        DropDown.startListeningToKeyboard()

        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = SplashScreenViewController(with: self)
        self.window?.makeKeyAndVisible()
        IQKeyboardManager.sharedManager().enable = true
        
        controlsUniversalAppearance()
        configureSideMenu()
        
        GMSServices.provideAPIKey("AIzaSyC6Tj8HzuwX2FuImjc-yELwFr9RKtF6kY0")
        GMSPlacesClient.provideAPIKey("AIzaSyC6Tj8HzuwX2FuImjc-yELwFr9RKtF6kY0")
        
        return true
    }
    
    private func configureSideMenu() {
        SideMenuController.preferences.basic.menuWidth = 375
        SideMenuController.preferences.basic.defaultCacheKey = "0"
    }
    
    private func controlsUniversalAppearance(){
        UITabBarItem.appearance()
            .setTitleTextAttributes(
                [NSAttributedStringKey.font: UIFont(name: "Times new Roman", size: 12)!],
                for: .normal)
        
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.font.rawValue: UIFont.systemFont(ofSize: 13.0)]
        
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK: Splash Screen Delegate methods
    
    func splashScreenDone() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let startingController = storyBoard.instantiateViewController(withIdentifier: "MediSideMenuController") as? SideMenuController
            self.window?.rootViewController = startingController
        }
    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


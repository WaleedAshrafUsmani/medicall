

import Foundation
import Alamofire
import PromiseKit
import ObjectMapper

 class ApiManager {
    
    let baseUrl = "http://themedicall.com/medi-app-api/"
    let b = "http://themedicall.com/medi-app-api/is-username-unique/Vx0cbjkzfQpyTObY8vfqgN1us"
    
    static let shared = ApiManager()
    private init()
    {
        
    }
    
    
    func fetchDataFromAPi() -> Promise<[Specilatie]>{
        return Promise<[Specilatie]> {
            fufil,reject -> Void in
            return
             Alamofire.request("http://themedicall.com/medi-app-api/get-specialities/Vx0cbjkzfQpyTObY8vfqgN1us").responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                   // print(responseString)
                    let doctorResponse = DoctorType(JSONString:"\(responseString)")!
                    fufil(doctorResponse.specilaities!)
                case .failure(let Error):
                    //print(Error)
                    reject(Error)
                }
                
            }
        }
        
        
        
        
    }
    
    func fetchAllDoctors(id:Int,offset:Int) -> Promise<[Doctors]>{
        let url = baseUrl + "get-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "speciality_id" : id,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        return Promise<[Doctors]> {
            fufil,reject -> Void in
            return

             Alamofire.request(url,method:.post,parameters:params).responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                     print(responseString)
                    let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                     if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                        reject(NSError.init(domain: "", code: 0, userInfo: nil))
                     }
                    fufil(doctorResponse.doctors!)
                case .failure(let Error):
                    //print(Error)
                    reject(Error)
                }

            }
        }


    }
    
    func tryFetchAllDoctors(id:Int,offset:Int, success:  @escaping (_ user: AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "speciality_id" : id,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        
            
            Alamofire.request(url,method:.post,parameters:params).responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    print(responseString)
                    let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                    if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                        failure(NSError.init(domain: "", code: 0, userInfo: nil))
                    }
                    success(doctorResponse)
                case .failure(let error):
                    failure(error as NSError)
                }
                
            }
        
        
        
    }
    func tryFetchHospitalDetails(parameters : [String: Any], success:  @escaping (_ hospitalDetails : Hospital?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-hospital-details"
        Alamofire.request(url,method:.post,parameters:parameters).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let myResponse = RootClass(JSONString:"\(responseString)")!
                
                if myResponse.hospital == nil {
                    failure(NSError.init(domain: "Unable to parse", code: 0, userInfo: [:]))
                }
                
                success(myResponse.hospital!)
                
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
    }
    
    func tryFetchDoctorPracticeDetails(parameters : [String: Any], success:  @escaping (_ doctor : AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-doctor-practice-details"
        Alamofire.request(url,method:.post,parameters:parameters).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
                }
                success(doctorResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
    }
    func tryFetchDoctorBioDetails(parameters : [String: Any], success:  @escaping (_ doctor : AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-doctor-bio-details"
        Alamofire.request(url,method:.post,parameters:parameters).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
                }
                success(doctorResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
        
        
        
    }
    
    
    func tryFetchPanelDoctors(id:Int,offset:Int, success:  @escaping (_ user: AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-hospital-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "hospital_id" : id,
            "offset":offset
        ]
        
        
        Alamofire.request(url,method:.post,parameters:params).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
                }
                success(doctorResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
        
        
        
    }
    
    
    func tryFetchNearByDoctors(id:Int,offset:Int, success:  @escaping (_ user: AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-nearby-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "speciality_id" : id,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        
        
        Alamofire.request(url,method:.post,parameters:params).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
                }
                success(doctorResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
        
        
        
    }
    func tryFetchDiscountedDoctors(id:Int,offset:Int, success:  @escaping (_ user: AllDoctor?) -> Void, failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-discounted-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "speciality_id" : id,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        
        
        Alamofire.request(url,method:.post,parameters:params).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
                }
                success(doctorResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
        
        
        
    }

    
    func fetchDiscountedDoctors(id:Int,offset:Int) -> Promise<[Doctors]> {
        let url = baseUrl + "get-discounted-doctors"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "speciality_id" : id,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        return Promise<[Doctors]> {
            fufil,reject -> Void in
            return
            
            Alamofire.request(url,method:.post,parameters:params).responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    print(responseString)
                    let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                    fufil(doctorResponse.doctors!)
                case .failure(let Error):
                    //print(Error)
                    reject(Error)
                }
                
            }
        }
        
    }
    
    func fetchDoctorPracticeDetail(id:Int,udid:String) -> Promise<[Doctors]> {
        let url = baseUrl + "get-doctor-practice-details"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "doctor_id" : id,
            "uniqueId" : udid,
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            //"offset":offset
        ]
        return Promise<[Doctors]> {
            fufil,reject -> Void in
            return
            
             Alamofire.request(url,method:.post,parameters:params).responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    print(responseString)
                    let doctorResponse = AllDoctor(JSONString:"\(responseString)")!
                    fufil([doctorResponse.doctor!])
                case .failure(let Error):
                    //print(Error)
                    reject(Error)
                }
                
            }
        }
        
    }
    
    func getCities () -> Promise<[Cities]> {
        let url = "http://themedicall.com/medi-app-api/get-cities/Vx0cbjkzfQpyTObY8vfqgN1us"
        return Promise <[Cities]> {
            fulfil,reject -> Void in
            return
            
             Alamofire.request(url,method:.get).responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    print(responseString)
                    let cities = AllCities(JSONString:"\(responseString)")!
                    fulfil(cities.cities!)
                case .failure(let Error):
                    //print(Error)
                    reject(Error)
                }
                
            }
        }
    }
    
    
    
    func loginUser(email: String, password: String, success:  @escaping (_ user: User) -> Void, failure: @escaping (_ error: CustomError) -> Void) {
        let url = "http://themedicall.com/medi-app-api/login"
        let parameter: Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "text" : email,
                "password": password
            
            ]
        Alamofire.request(
            url,
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.httpBody)
            .responseJSON { (response) -> Void in
                print(response.result.value!)
                guard response.result.isSuccess else {
                    let error = CustomError("Server Error")
                    failure(error)
                    return
                }
                
                if let err = Mapper<CustomError>().map(JSONObject: response.result.value) {
                    failure(err)
                    return
                }
                
                if let user = Mapper<User>().map(JSONObject: response.result.value) {
                    success(user)
                    return
                }
                
                let error = CustomError("Server Error")
                failure(error)
        }
    }
    
    func loginUser2(email:String,password:String) -> Promise<User> {
        let url = "http://themedicall.com/medi-app-api/login"
        let parameter: Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "text" : email,
                "password": password
                
        ]
        return Promise<User> {
            fulfil,Reject -> Void in
            return
        
        Alamofire.request(
            url,
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.httpBody)
            .responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    print(responseString)
                    let allUser = User(JSONString:"\(responseString)")!
                    fulfil(allUser)
                case .failure(let error):
                    print(error)
                }
            }
    }
    }
    
    func isUserValid(userName:String) -> Promise<UserValidation>  {
        let url = "http://themedicall.com/medi-app-api/is-username-unique"
        let parameter:Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "user_name" : userName
        ]
        return Promise<UserValidation> {
            fulfil,Reject -> Void in
            return
                   Alamofire.request(
                url,
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.httpBody)
                .responseString {
                    response in
                    switch (response.result) {
                    case .success(let responseString):
                        let correctUser = UserValidation(JSONString:"\(responseString)")!
                        fulfil(correctUser)
                    case .failure(let error):
                        print(error)
                    }
            }
            
        }
    }
    
    func signUpUser(userName:String,fullName:String,phoneNo:String,City:Int,Gender:String,Speciality:Int,Email:String,Password:String,PMDC:String,dob:String) -> Promise<RegisterDoctor>{
        let url = "http://themedicall.com/medi-app-api/register-doctor"
        let parameter:Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "username" : userName,
                "fullname" : fullName,
                "mobilenumber" : phoneNo,
                "dob" : dob,
                "city" : City,
                "gender" : Gender,
                "email" : Email,
                "password" : Password,
                "experience_status_id" : Speciality,
                "pmdc_number" : PMDC
        ]
        return Promise<RegisterDoctor> {
            fulfil,Reject -> Void in
//            return
            Alamofire.request(
                url,
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.httpBody)
                .responseString {
                    response in
                    switch (response.result) {
                    case .success(let responseString):
                        let correctUser = RegisterDoctor(JSONString:"\(responseString)")!
                        fulfil(correctUser)
                    case .failure(let error):
                        print(error)
                    }
            }
            
        }
    }
    
    func trySignup(with parameter : [String : Any], success : @escaping ((RegisterDoctor)->Void), failureBlock failure: @escaping ((_ error: NSError?) -> Void)){
        let url = "http://themedicall.com/medi-app-api/register-doctor"
        
        Alamofire.request(
            url,
            method: .post,
            parameters: parameter,
            encoding: URLEncoding.httpBody)
            .responseString {
                response in
                switch (response.result) {
                case .success(let responseString):
                    let correctUser = RegisterDoctor(JSONString:"\(responseString)")!
                    if correctUser.error! == true {
                        failure(NSError.init(domain: correctUser.errorMessage!, code: 404, userInfo: nil))
                    }
                    else {
                        success(correctUser)
                    }
                    
                case .failure(let error):
                    failure(error as NSError)
                }
        }
        
    }
    
    func verifyUser(userId:Int) -> Promise<User>  {
        let url = "http://themedicall.com/medi-app-api/verify-user"
        let parameter:Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "user_id" : userId
        ]
        return Promise<User> {
            fulfil,Reject -> Void in
            return
             Alamofire.request(
                url,
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.httpBody)
                .responseString {
                    response in
                    switch (response.result) {
                    case .success(let responseString):
                        print(responseString)
                        let correctUser = User(JSONString:"\(responseString)")!
                        fulfil(correctUser)
                    case .failure(let error):
                        print(error)
                    }
            }
            
        }
    }
    func uploadImage(doctorId:Int,PmdcPic:URL) {
        let url = "http://themedicall.com/medi-app-api/upload-doctor-pmdc-picture"
        let parameter:Parameters = [
            "doctor_id":14445,
             "key" : "Vx0cbjkzfQpyTObY8vfqgN1us"
        ]
    
        
    }
    
    
    
//    func getDoctor (success: @escaping(_ doctor:SingleDoctor) -> Void)  {
//        let url = "http://themedicall.com/medi-app-api/get-doctor-bio-details?key=Vx0cbjkzfQpyTObY8vfqgN1us&doctor_id=12532"
//
//            Alamofire.request(url,method:.post).responseString
//                {
//                response in
//                switch (response.result) {
//                case .success(let responseString):
//                    print(responseString)
//                    if let Sdoctor = Mapper<SingleDoctor>.map(responseString)
//                    {
//                    success(Sdoctor)
//                    return
//                    }
//
//                case .failure(let Error):
//                    print("some error")
//
//                }
//
//            }
    
    
    
    func registerationNonMedic(userName:String,fullName:String,phoneNo:String,City:Int,Gender:String,bloodGroup:Int,Email:String,Password:String) -> Promise<RegisterDoctor>{
        let url = "http://themedicall.com/medi-app-api/register-patient"
        let parameter:Parameters =
            [
                "key" : "Vx0cbjkzfQpyTObY8vfqgN1us",
                "username" : userName,
                "fullname" : fullName,
                "mobilenumber" : phoneNo,
                "city" : City,
                "gender" : Gender,
                "email" : Email,
                "password" : Password,
                "bloodgroup" : bloodGroup,
                "social_id" : ""
        ]
        return Promise<RegisterDoctor> {
            fulfil,Reject -> Void in
            return
            Alamofire.request(
                url,
                method: .post,
                parameters: parameter,
                encoding: URLEncoding.httpBody)
                .responseString {
                    response in
                    switch (response.result) {
                    case .success(let responseString):
                        let correctUser = RegisterDoctor(JSONString:"\(responseString)")!
                        fulfil(correctUser)
                    case .failure(let error):
                        print(error)
                    }
            }
            
        }
    }
    
    func tryFetchAllHospital(id:Int,offset:Int, success:  @escaping ((HospitalTopResponse?) -> Void), failure: @escaping (_ error: NSError) -> Void){
        let url = baseUrl + "get-hospitals"
        let params: Parameters = [
            "key": "Vx0cbjkzfQpyTObY8vfqgN1us",
            "city" : "Lahore",
            "lat" : "31.373290",
            "lng" : "74.187274",
            "offset":offset
        ]
        
        
        Alamofire.request(url,method:.post,parameters:params).responseString {
            response in
            switch (response.result) {
            case .success(let responseString):
                print(responseString)
                let hospitalResponse = HospitalTopResponse(JSONString:"\(responseString)")!
//                if doctorResponse.doctors == nil || doctorResponse.doctors!.count == 0 {
//                    failure(NSError.init(domain: "", code: 0, userInfo: nil))
//                }
                success(hospitalResponse)
            case .failure(let error):
                failure(error as NSError)
            }
            
        }
        
        
        
    }
    
}
    
    
    
    
    
    
    


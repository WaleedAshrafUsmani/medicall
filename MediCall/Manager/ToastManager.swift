
import Foundation
import SwiftMessageBar

class ToastManager {
    static func showErrorMessage(message: String) {
        SwiftMessageBar.showMessageWithTitle("Error", message: message, type: .error)
    }
    
    static func showSuccessMessage(message: String) {
        SwiftMessageBar.showMessageWithTitle("Success", message: message, type: .success)
    }
    
    static func showInfoMessage(message: String) {
        SwiftMessageBar.showMessageWithTitle("Information", message: message, type: .info)
    }
    
}



import Foundation

class UserDefaultManager {
    
    public static let shared = UserDefaultManager()
    
    private init () {
        
    }
    
    func writeUser(_ user:User) {
        let defaults = UserDefaults.standard
        defaults.set(user.userName, forKey: "user.Name")
        defaults.set(user.userFullName, forKey: "user.FullName")
        defaults.set(user.userEmail, forKey: "user.Email")
        defaults.set(user.userId, forKey: "user.id")
        defaults.set(user.userPhone, forKey: "user.phone")
        defaults.set(user.userTable, forKey: "user.Table")
        defaults.set(user.verifiedStatus, forKey: "user")
        defaults.set(user.profileImage, forKey: "Profile")
        defaults.synchronize()
    }
    
    func readUser() -> User {
       
        let defaults = UserDefaults.standard
        let user = User()
        
        user.userName = defaults.value(forKey: "user.Name") as? String
        user.userFullName = defaults.value(forKey: "user.FullName") as? String
        user.userEmail = defaults.value(forKey: "user.Email") as? String
        user.userId = defaults.value(forKey: "user.id") as? Int
        user.userPhone = defaults.value(forKey: "user.phone") as? String
        user.userTable = defaults.value(forKey: "user.Table") as? String
        user.verifiedStatus = defaults.value(forKey: "user") as? String
        user.profileImage = defaults.value(forKey: "Profile") as? String
        
        return user
    }
    
    func removeUser() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey:"user.Name")
        defaults.removeObject(forKey:"user.FullName")
        defaults.removeObject(forKey:"user.Email")
        defaults.removeObject(forKey:"user.id")
        defaults.removeObject(forKey:"user.phone")
        defaults.removeObject(forKey:"user.Table")
        defaults.removeObject(forKey:"user")
        defaults.removeObject(forKey: "Profile")
        defaults.synchronize()
    }
}

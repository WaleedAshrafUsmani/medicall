
import Foundation
import ObjectMapper

class AllDoctor:Mappable {
    
    var doctors:[Doctors]?
    var doctor:Doctors?
    
    var total : Int!
    var error_message : String!
    var error : Bool = false
    
    init() {
        
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        doctors <- map["doctors"]
        doctor <- map["doctors"]
        total <- map["total"]
        error_message <- map["error_message"]
        error <- map["error"]
        
    }
}

class Doctors:Mappable {
    var doctor_id:Int?
    var doctor_full_name:String?
    var doctor_mobile:Double?
    var doctor_min_fee:String?
    var docor_max_fee:String?
    var doctor_img:String?
    var doctor_experience:String?
    var doctor_verified_status:Int?
    var doctor_offer_any_discount:String?
    var experience_status:ExperienceStatus?
    var speciality:[Speciality]?
    var qualifications:[Qualification]?
    var hospitals:[Hospitals]?
    var ratings:[Ratings]?
    var services:[Services]?
    var doctor_about_me : String?
    var registration:[Registeration]?
    
    init() {
        
    }
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        doctor_id <- map["doctor_id"]
        doctor_full_name <- map["doctor_full_name"]
        doctor_mobile <- map["doctor_mobile"]
        doctor_min_fee  <- map["doctor_min_fee"]
        docor_max_fee <- map["doctor_max_fee"]
        doctor_img <- map["doctor_img"]
        experience_status <- map["experience_status"]
        speciality <- map["speciality"]
        qualifications <- map["qualifications"]
        hospitals <- map["hospitals"]
        ratings <- map["ratings"]
        doctor_experience <- map["doctor_experience"]
        doctor_verified_status <- map["doctor_verified_status"]
        doctor_offer_any_discount <- map["doctor_offer_any_discount"]
        services <- map["services"]
        doctor_about_me <- map["doctor_about_me"]
        registration <- map["registration"]
    }
    
}
    
    
class ExperienceStatus:Mappable {
        var experience_status_id:Int?
        var experience_status_title:String?
        init() {
            
        }
        required init?(map: Map) {
        }
        
        func mapping(map: Map) {
            experience_status_id <- map["experience_status_id"]
            experience_status_title <- map["experience_status_title"]
        }
    }
    
class Speciality: Mappable {
        
    var speciality_id:Int?
    var doctor_id:Int?
    var speciality:Speciality2?
        
        init() {
            
        }
        required init?(map: Map) {
            
        }
        
        func mapping(map: Map) {
            speciality_id <- map["speciality_id"]
            doctor_id <- map["doctor_id"]
            speciality <- map["speciality"]
            
            
        }
        
        
    }
    
class  Speciality2: Mappable {
    var speciality_designation:String?
    var speciality_id:Int?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        speciality_designation <- map["speciality_designation"]
        speciality_id <- map["speciality_id"]
    }
    
    
}

class Qualification: Mappable {
    var qualification_id:Int?
    var doctor_id:Int?
    var qualifications:Qualification2?
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        qualification_id <- map["qualification_id"]
        doctor_id <- map["doctor_id"]
        qualifications <- map["qualifications"]
    }
    
    
}




class Hospitals:Mappable {
    var hospital_id:Int?
    var doctor_id:Int?
    var hospitals:NestedHospitals?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        hospital_id <- map["hospital_id"]
        doctor_id <- map["doctor_id"]
        hospitals <- map["hospitals"]
    }
    
    
}
class HospitalTopResponse:Mappable {
    var total : Int?
    var hospitals : [NestedHospitals]?
    
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        hospitals <- map["hospitals"]
    }
    
    
}
class DoctorInHospital:Mappable {
    var doctor_id : Int?
    var doctor_full_name : String?
    var doctor_img : String?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        doctor_id <- map["doctor_id"]
        doctor_full_name <- map["doctor_full_name"]
        doctor_img <- map["doctor_img"]
    }
    
    
}
class DoctorInHospitalOuter:Mappable {
    var doctor_id : Int?
    var hospital_id : Int?
    var doctors : DoctorInHospital?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        doctor_id <- map["doctor_id"]
        hospital_id <- map["hospital_id"]
        doctors <- map["doctors"]
    }
    
    
}

class NestedHospitals: Mappable {
    
    var hospital_id:Int?
    var hospital_name:String?
    var hospital_img:String?
    var hospital_lat:String?
    var hospital_lng:String?
    var landline:[LandLine]?
    var practices:[Practices]?
    
    
    var hospital_addr : String?
    var hospital_offer_any_discount : String?
    var hospital_share_url : String?
    var hospital_views : String?
    var hospital_panels : Int?
    var doctors:[DoctorInHospitalOuter]?
    
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        hospital_id <- map["hospital_id"]
        hospital_name <- map["hospital_name"]
        hospital_img <- map["hospital_img"]
        hospital_lat <- map["hospital_lat"]
        hospital_lng <- map["hospital_lng"]
        hospital_addr <- map["hospital_addr"]
        hospital_panels <- map["hospital_panels"]
        
        landline <- map["landline"]
        practices <- map["practices"]
        doctors <- map["doctors"]
    }
    
    
}

class LandLine: Mappable {
    var hospital_id:Int?
    var hospital_landline_number:String?
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        hospital_id <- map["hospital_id"]
        hospital_landline_number <- map["hospital_landline_number"]
    }
    
    
}

class Ratings: Mappable {
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Qualification2: Mappable {
    
    var qualification_id:Int?
    var qualification_title:String?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        qualification_id <- map["qualification_id"]
        qualification_title <- map["qualification_title"]
    }
    
    
}

class Services: Mappable {
    
    var service_id:Int?
    var doctor_id:Int?
    var services:Services2?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        service_id <- map["service_id"]
        doctor_id <- map["doctor_id"]
        services <- map["services"]
        
    }
    
    
}

class Practices: Mappable {
    
    var hospital_id:Int?
    var doctor_id: Int?
    var doctor_timing_start_time:String?
    var doctor_timing_end_time:String?
    var week_day_id:String?
    var week_days:WeekDays?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        hospital_id <- map["hospital_id"]
        doctor_id <- map["doctor_id"]
        doctor_timing_start_time <- map["doctor_timing_start_time"]
        doctor_timing_end_time <- map["doctor_timing_end_time"]
        week_day_id <- map["week_day_id"]
        week_days <- map["week_days"]
    }
    
    
}


class WeekDays: Mappable {
    
    var week_day_id:Int?
    var week_day_title:String?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        week_day_id <- map["week_day_id"]
        week_day_title <- map["week_day_title"]
    }
    
    
}

class Services2: Mappable {
    
    var service_id:Int?
    var service_title:String?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
     func mapping(map: Map) {
        service_id <- map["service_id"]
        service_title <- map["service_title"]
    }
    
    
}
















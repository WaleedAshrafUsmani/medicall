
import Foundation
import ObjectMapper

class CustomError : Mappable {
    var message: String?
    
    init(_ message: String) {
        self.message = message
    }
    
    required init?(map: Map) {
        let requiredKey = "error"
        let keys = Array(map.JSON.keys)
        
        if !keys.contains(requiredKey) {
            return nil
        }
    }
    
    func mapping(map: Map) {
        message <- map["error"]
    }
}

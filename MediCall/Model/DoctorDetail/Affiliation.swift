
import Foundation 
import ObjectMapper


class Affiliation : NSObject, NSCoding, Mappable{

	var affiliationId : Int?
	var affiliationTitle : String?
	var affiliations : Affiliation?
	var hospitalId : Int?


	class func newInstance(map: Map) -> Mappable?{
		return Affiliation()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		affiliationId <- map["affiliation_id"]
		affiliationTitle <- map["affiliation_title"]
		affiliations <- map["affiliations"]
		hospitalId <- map["hospital_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         affiliationId = aDecoder.decodeObject(forKey: "affiliation_id") as? Int
         affiliationTitle = aDecoder.decodeObject(forKey: "affiliation_title") as? String
         affiliations = aDecoder.decodeObject(forKey: "affiliations") as? Affiliation
         hospitalId = aDecoder.decodeObject(forKey: "hospital_id") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if affiliationId != nil{
			aCoder.encode(affiliationId, forKey: "affiliation_id")
		}
		if affiliationTitle != nil{
			aCoder.encode(affiliationTitle, forKey: "affiliation_title")
		}
		if affiliations != nil{
			aCoder.encode(affiliations, forKey: "affiliations")
		}
		if hospitalId != nil{
			aCoder.encode(hospitalId, forKey: "hospital_id")
		}

	}

}


import Foundation 
import ObjectMapper


class Area : NSObject, NSCoding, Mappable{

	var areaId : Int?
	var areaTitle : String?


	class func newInstance(map: Map) -> Mappable?{
		return Area()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		areaId <- map["area_id"]
		areaTitle <- map["area_title"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         areaId = aDecoder.decodeObject(forKey: "area_id") as? Int
         areaTitle = aDecoder.decodeObject(forKey: "area_title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if areaId != nil{
			aCoder.encode(areaId, forKey: "area_id")
		}
		if areaTitle != nil{
			aCoder.encode(areaTitle, forKey: "area_title")
		}

	}

}

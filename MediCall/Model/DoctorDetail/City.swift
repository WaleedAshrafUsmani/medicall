
import Foundation 
import ObjectMapper


class City : NSObject, NSCoding, Mappable{

	var cityId : Int?
	var cityTitle : String?


	class func newInstance(map: Map) -> Mappable?{
		return City()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		cityId <- map["city_id"]
		cityTitle <- map["city_title"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cityId = aDecoder.decodeObject(forKey: "city_id") as? Int
         cityTitle = aDecoder.decodeObject(forKey: "city_title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if cityTitle != nil{
			aCoder.encode(cityTitle, forKey: "city_title")
		}

	}

}

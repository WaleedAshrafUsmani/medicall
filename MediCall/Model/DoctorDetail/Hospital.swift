
import Foundation 
import ObjectMapper


class Hospital : NSObject, NSCoding, Mappable{

	var affiliations : [Affiliation]?
	var area : Area?
	var areaId : Int?
	var city : City?
	var cityId : Int?
	var hospitalAboutUs : String?
	var hospitalAddr : String?
	var hospitalAddress : String?
	var hospitalCloseTiming : String?
	var hospitalContact : AnyObject?
	var hospitalCoverImg : String?
	var hospitalEmail : String?
	var hospitalId : Int?
	var hospitalImg : String?
	var hospitalLat : String?
	var hospitalLng : String?
	var hospitalName : String?
	var hospitalOpenTiming : String?
	var hospitalShareUrl : String?
	var hospitalTimingStatus : String?
	var hospitalTotalBeds : Int?
	var hospitalUpdatedAt : String?
	var hospitalUrl : String?
	var hospitalViews : Int?
	var insurances : [AnyObject]?
	var landline : [Landline]?
	var services : [Service]?
	var workingDays : [AnyObject]?


	class func newInstance(map: Map) -> Mappable?{
		return Hospital()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		affiliations <- map["affiliations"]
		area <- map["area"]
		areaId <- map["area_id"]
		city <- map["city"]
		cityId <- map["city_id"]
		hospitalAboutUs <- map["hospital_about_us"]
		hospitalAddr <- map["hospital_addr"]
		hospitalAddress <- map["hospital_address"]
		hospitalCloseTiming <- map["hospital_close_timing"]
		hospitalContact <- map["hospital_contact"]
		hospitalCoverImg <- map["hospital_cover_img"]
		hospitalEmail <- map["hospital_email"]
		hospitalId <- map["hospital_id"]
		hospitalImg <- map["hospital_img"]
		hospitalLat <- map["hospital_lat"]
		hospitalLng <- map["hospital_lng"]
		hospitalName <- map["hospital_name"]
		hospitalOpenTiming <- map["hospital_open_timing"]
		hospitalShareUrl <- map["hospital_share_url"]
		hospitalTimingStatus <- map["hospital_timing_status"]
		hospitalTotalBeds <- map["hospital_total_beds"]
		hospitalUpdatedAt <- map["hospital_updated_at"]
		hospitalUrl <- map["hospital_url"]
		hospitalViews <- map["hospital_views"]
		insurances <- map["insurances"]
		landline <- map["landline"]
		services <- map["services"]
		workingDays <- map["working_days"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         affiliations = aDecoder.decodeObject(forKey: "affiliations") as? [Affiliation]
         area = aDecoder.decodeObject(forKey: "area") as? Area
         areaId = aDecoder.decodeObject(forKey: "area_id") as? Int
         city = aDecoder.decodeObject(forKey: "city") as? City
         cityId = aDecoder.decodeObject(forKey: "city_id") as? Int
         hospitalAboutUs = aDecoder.decodeObject(forKey: "hospital_about_us") as? String
         hospitalAddr = aDecoder.decodeObject(forKey: "hospital_addr") as? String
         hospitalAddress = aDecoder.decodeObject(forKey: "hospital_address") as? String
         hospitalCloseTiming = aDecoder.decodeObject(forKey: "hospital_close_timing") as? String
         hospitalContact = aDecoder.decodeObject(forKey: "hospital_contact") as? AnyObject
         hospitalCoverImg = aDecoder.decodeObject(forKey: "hospital_cover_img") as? String
         hospitalEmail = aDecoder.decodeObject(forKey: "hospital_email") as? String
         hospitalId = aDecoder.decodeObject(forKey: "hospital_id") as? Int
         hospitalImg = aDecoder.decodeObject(forKey: "hospital_img") as? String
         hospitalLat = aDecoder.decodeObject(forKey: "hospital_lat") as? String
         hospitalLng = aDecoder.decodeObject(forKey: "hospital_lng") as? String
         hospitalName = aDecoder.decodeObject(forKey: "hospital_name") as? String
         hospitalOpenTiming = aDecoder.decodeObject(forKey: "hospital_open_timing") as? String
         hospitalShareUrl = aDecoder.decodeObject(forKey: "hospital_share_url") as? String
         hospitalTimingStatus = aDecoder.decodeObject(forKey: "hospital_timing_status") as? String
         hospitalTotalBeds = aDecoder.decodeObject(forKey: "hospital_total_beds") as? Int
         hospitalUpdatedAt = aDecoder.decodeObject(forKey: "hospital_updated_at") as? String
         hospitalUrl = aDecoder.decodeObject(forKey: "hospital_url") as? String
         hospitalViews = aDecoder.decodeObject(forKey: "hospital_views") as? Int
         insurances = aDecoder.decodeObject(forKey: "insurances") as? [AnyObject]
         landline = aDecoder.decodeObject(forKey: "landline") as? [Landline]
         services = aDecoder.decodeObject(forKey: "services") as? [Service]
         workingDays = aDecoder.decodeObject(forKey: "working_days") as? [AnyObject]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if affiliations != nil{
			aCoder.encode(affiliations, forKey: "affiliations")
		}
		if area != nil{
			aCoder.encode(area, forKey: "area")
		}
		if areaId != nil{
			aCoder.encode(areaId, forKey: "area_id")
		}
		if city != nil{
			aCoder.encode(city, forKey: "city")
		}
		if cityId != nil{
			aCoder.encode(cityId, forKey: "city_id")
		}
		if hospitalAboutUs != nil{
			aCoder.encode(hospitalAboutUs, forKey: "hospital_about_us")
		}
		if hospitalAddr != nil{
			aCoder.encode(hospitalAddr, forKey: "hospital_addr")
		}
		if hospitalAddress != nil{
			aCoder.encode(hospitalAddress, forKey: "hospital_address")
		}
		if hospitalCloseTiming != nil{
			aCoder.encode(hospitalCloseTiming, forKey: "hospital_close_timing")
		}
		if hospitalContact != nil{
			aCoder.encode(hospitalContact, forKey: "hospital_contact")
		}
		if hospitalCoverImg != nil{
			aCoder.encode(hospitalCoverImg, forKey: "hospital_cover_img")
		}
		if hospitalEmail != nil{
			aCoder.encode(hospitalEmail, forKey: "hospital_email")
		}
		if hospitalId != nil{
			aCoder.encode(hospitalId, forKey: "hospital_id")
		}
		if hospitalImg != nil{
			aCoder.encode(hospitalImg, forKey: "hospital_img")
		}
		if hospitalLat != nil{
			aCoder.encode(hospitalLat, forKey: "hospital_lat")
		}
		if hospitalLng != nil{
			aCoder.encode(hospitalLng, forKey: "hospital_lng")
		}
		if hospitalName != nil{
			aCoder.encode(hospitalName, forKey: "hospital_name")
		}
		if hospitalOpenTiming != nil{
			aCoder.encode(hospitalOpenTiming, forKey: "hospital_open_timing")
		}
		if hospitalShareUrl != nil{
			aCoder.encode(hospitalShareUrl, forKey: "hospital_share_url")
		}
		if hospitalTimingStatus != nil{
			aCoder.encode(hospitalTimingStatus, forKey: "hospital_timing_status")
		}
		if hospitalTotalBeds != nil{
			aCoder.encode(hospitalTotalBeds, forKey: "hospital_total_beds")
		}
		if hospitalUpdatedAt != nil{
			aCoder.encode(hospitalUpdatedAt, forKey: "hospital_updated_at")
		}
		if hospitalUrl != nil{
			aCoder.encode(hospitalUrl, forKey: "hospital_url")
		}
		if hospitalViews != nil{
			aCoder.encode(hospitalViews, forKey: "hospital_views")
		}
		if insurances != nil{
			aCoder.encode(insurances, forKey: "insurances")
		}
		if landline != nil{
			aCoder.encode(landline, forKey: "landline")
		}
		if services != nil{
			aCoder.encode(services, forKey: "services")
		}
		if workingDays != nil{
			aCoder.encode(workingDays, forKey: "working_days")
		}

	}

}

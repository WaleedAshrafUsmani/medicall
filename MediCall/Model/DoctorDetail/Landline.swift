
import Foundation 
import ObjectMapper


class Landline : NSObject, NSCoding, Mappable{

	var hospitalId : Int?
	var hospitalLandlineNumber : String?


	class func newInstance(map: Map) -> Mappable?{
		return Landline()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		hospitalId <- map["hospital_id"]
		hospitalLandlineNumber <- map["hospital_landline_number"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         hospitalId = aDecoder.decodeObject(forKey: "hospital_id") as? Int
         hospitalLandlineNumber = aDecoder.decodeObject(forKey: "hospital_landline_number") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if hospitalId != nil{
			aCoder.encode(hospitalId, forKey: "hospital_id")
		}
		if hospitalLandlineNumber != nil{
			aCoder.encode(hospitalLandlineNumber, forKey: "hospital_landline_number")
		}

	}

}

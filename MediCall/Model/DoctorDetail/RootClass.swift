
import Foundation 
import ObjectMapper


class RootClass : NSObject, NSCoding, Mappable{

	var error : Bool?
	var hospital : Hospital?
	var totalDoctors : Int?


	class func newInstance(map: Map) -> Mappable?{
		return RootClass()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
		error <- map["error"]
		hospital <- map["hospital"]
		totalDoctors <- map["total_doctors"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         error = aDecoder.decodeObject(forKey: "error") as? Bool
         hospital = aDecoder.decodeObject(forKey: "hospital") as? Hospital
         totalDoctors = aDecoder.decodeObject(forKey: "total_doctors") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if error != nil{
			aCoder.encode(error, forKey: "error")
		}
		if hospital != nil{
			aCoder.encode(hospital, forKey: "hospital")
		}
		if totalDoctors != nil{
			aCoder.encode(totalDoctors, forKey: "total_doctors")
		}

	}

}

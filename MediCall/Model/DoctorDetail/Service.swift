
import Foundation 
import ObjectMapper


class Service : NSObject, NSCoding, Mappable{

//    var servicesHospitalId : Int?
	var servicesHospitalTitle : String?
	var hospitalId : Int?
	var services : Service?
	var servicesHospitalId : String?


	class func newInstance(map: Map) -> Mappable?{
		return Service()
	}
	required init?(map: Map){}
	private override init(){}

	func mapping(map: Map)
	{
//        servicesHospitalId <- map["services_hospital_id"]
		servicesHospitalTitle <- map["services_hospital_title"]
		hospitalId <- map["hospital_id"]
		services <- map["services"]
		servicesHospitalId <- map["services_hospital_id"]
		
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
//         servicesHospitalId = aDecoder.decodeObject(forKey: "services_hospital_id") as? Int
         servicesHospitalTitle = aDecoder.decodeObject(forKey: "services_hospital_title") as? String
         hospitalId = aDecoder.decodeObject(forKey: "hospital_id") as? Int
         services = aDecoder.decodeObject(forKey: "services") as? Service
         servicesHospitalId = aDecoder.decodeObject(forKey: "services_hospital_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
//        if servicesHospitalId != nil{
//            aCoder.encode(servicesHospitalId, forKey: "services_hospital_id")
//        }
		if servicesHospitalTitle != nil{
			aCoder.encode(servicesHospitalTitle, forKey: "services_hospital_title")
		}
		if hospitalId != nil{
			aCoder.encode(hospitalId, forKey: "hospital_id")
		}
		if services != nil{
			aCoder.encode(services, forKey: "services")
		}
		if servicesHospitalId != nil{
			aCoder.encode(servicesHospitalId, forKey: "services_hospital_id")
		}

	}

}


import Foundation
import ObjectMapper

class DoctorType:Mappable {
    
    var specilaities:[Specilatie]?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        specilaities <- map["specilaities"]
    }
}

class Specilatie:Mappable {
    
    var speciality_id:Int?
    var speciality_designation:String?
    var speciality_icon:String?
    
    init() {
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        speciality_id <- map["speciality_id"]
        speciality_designation <- map["speciality_designation"]
        speciality_icon <- map["speciality_icon"]
    }
    
    
}


import Foundation
import ObjectMapper

class RegisterDoctor:Mappable {
    
    var error:Bool?
    var userId:Int?
    var doctorId:Int?
    var errorMessage:String?
    var code:Int?
    
    
    init() {
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
      error <- map["error"]
      userId <- map["user_id"]
      doctorId <- map["doctor_id"]
      code <- map["code"]
      errorMessage <- map["error_message"]
    }
    
    
}

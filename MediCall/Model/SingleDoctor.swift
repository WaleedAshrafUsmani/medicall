
import Foundation
import ObjectMapper

class SingleDoctor:Mappable  {
    var doctors:Doctor?
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        doctors <- map["doctors"]
    }
    
    
}

class Doctor: Mappable {
    
    
    
    
    var doctor_id:Int?
    
    var doctor_full_name:String?
    var doctor_experience:String?
    var doctor_min_fee:String?
    var docor_max_fee:String?
    var doctor_gender:String?
    var experience_status_id:Int?
    var doctor_img:String?
    var doctor_cover_photo:String?
    var doctor_video:String?
    var doctor_about_me:String?
    var doctor_publications:String?
    var doctor_achievements:String?
    var doctor_extra_curricular_activities:String?
    var experience_status:Experience?
    var speciality:[SpecialityinDetail]?
    var sub_speciality:[SubSpeciality]?
    var expertise:[Expertise]?
    var registration:[Registeration]?
    var qualifications:[Qualifications]?
    var institutions:[Institutions]?
    var gallery:[Gallery]?
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        doctor_full_name <- map["doctor_full_name"]
        doctor_experience <- map["doctor_experience"]
        doctor_min_fee <- map["doctor_min_fee"]
        docor_max_fee <- map["docor_max_fee"]
        doctor_gender <- map["doctor_gender"]
        experience_status_id <- map["experience_status_id"]
        doctor_img <- map["doctor_img"]
        doctor_cover_photo <- map["doctor_cover_photo"]
        doctor_video <- map["doctor_video"]
        doctor_about_me <- map["doctor_about_me"]
        doctor_publications <- map["doctor_publications"]
        doctor_achievements <- map["doctor_achievements"]
        doctor_extra_curricular_activities <- map["doctor_extra_curricular_activities"]
        experience_status <- map["experience_status"]
        speciality <- map["speciality"]
        sub_speciality <- map["sub_speciality"]
        expertise <- map["expertise"]
        registration <- map["registration"]
        qualifications <- map["qualifications"]
        institutions <- map["institutions"]
        gallery <- map["gallery"]
    }
    
    
}

class Experience: Mappable {
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class SpecialityinDetail:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class SubSpeciality:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Expertise:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Registeration:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Qualifications:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Institutions:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}

class Gallery:Mappable{
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
    
}





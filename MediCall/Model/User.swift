
import Foundation
import ObjectMapper

class User: Mappable {
    
    var userName:String?
    var userId:Int?
    var userTable:String?
    var userEmail:String?
    var userPhone:String?
    var userFullName:String?
    var id:Int?
    var verifiedStatus:String?
    var experienceStatus:String?
    var profileImage:String?
    var errorMessage:String?
    var error:Bool?
    
//    "user_name": "shobi",
//    "user_id": 67,
//    "user_table": "patients",
//    "user_email": "al5i.gilani@yahoo.com",
//    "user_phone": "3200000000",
//    "full_name": "Shoaib Anwar",
//    "id": 9,
//    "verified_status": 0,
//    "profile_img": "patient.png",
//    "error_message": "Logged in",
//    "error": false
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        userName <- map["user_name"]
        userId <- map["user_id"]
        userTable <- map["user_table"]
        userEmail <- map["user_email"]
        userPhone <- map["user_phone"]
        userFullName <- map["full_name"]
        id <- map["id"]
        verifiedStatus <- map["verified_status"]
        profileImage <- map["profile_img"]
        errorMessage <- map["error_message"]
        error <- map["error"]
        experienceStatus <- map["experience_status"]
    }
    
    
}

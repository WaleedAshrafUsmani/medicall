
import Foundation
import ObjectMapper

class UserValidation:Mappable {
    
    var Error:Bool?
    var errorMessage:String?
    
    
    init() {
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map)
    {
       Error <- map["error"]
       errorMessage <- map["error_message"]
    }
    
    
}
